<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvadisenoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovadiseno', function(Blueprint $table)
		{
			$table->integer('id')->unique('id');
			$table->integer('numopcfilas');
			$table->text('colorprincipal', 65535);
			$table->text('colorsecundario', 65535);
			$table->text('colorterciario', 65535);
			$table->text('colorderesalte', 65535);
			$table->text('colordeletraresalte', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovadiseno');
	}

}
