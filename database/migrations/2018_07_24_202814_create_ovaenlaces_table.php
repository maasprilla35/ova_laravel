<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaenlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaenlaces', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('titulo', 150);
			$table->string('url', 250)->unique('url');
			$table->integer('id_tema');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaenlaces');
	}

}
