<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaseleccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaseleccion', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('enunciado', 1000);
			$table->string('opcion1', 250);
			$table->string('opcion2', 250);
			$table->string('opcion3', 250);
			$table->string('opcion4', 250);
			$table->smallInteger('opcioncorrecta');
			$table->smallInteger('tipo');
			$table->smallInteger('numopciones');
			$table->smallInteger('valor');
			$table->smallInteger('orden');
			$table->smallInteger('id_tema');
			$table->smallInteger('numerogrupo');
			$table->smallInteger('status');
			$table->smallInteger('categoria');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaseleccion');
	}

}
