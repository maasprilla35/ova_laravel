<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaasignaturaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaasignatura', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('codigo', 20);
			$table->string('nombre', 100);
			$table->string('prerequisitos');
			$table->text('programa', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaasignatura');
	}

}
