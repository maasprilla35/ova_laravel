<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvabibliografiaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovabibliografia', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('titulo', 150);
			$table->string('autor', 150);
			$table->string('info', 500);
			$table->integer('id_tema');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovabibliografia');
	}

}
