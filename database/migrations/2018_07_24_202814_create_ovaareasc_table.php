<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaareascTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaareasc', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('titulo', 150)->unique('titulo');
			$table->integer('status');
			$table->smallInteger('orden');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaareasc');
	}

}
