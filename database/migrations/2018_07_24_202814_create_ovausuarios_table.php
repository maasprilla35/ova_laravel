<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvausuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovausuarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nick', 30)->unique('nick');
			$table->string('password');
			$table->string('nombres', 50);
			$table->string('apellidos', 50);
			$table->string('cedula', 12)->unique('cedula');
			$table->string('email', 80)->unique('email');
			$table->string('profesion', 80)->nullable();
			$table->string('carrera', 80)->nullable();
			$table->string('semestre', 15)->nullable();
			$table->integer('id_tipo');
			$table->integer('status')->default(0);
			$table->dateTime('last_session')->nullable();
			$table->string('token', 40)->nullable();
			$table->string('token_password', 100)->nullable();
			$table->integer('password_request')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovausuarios');
	}

}
