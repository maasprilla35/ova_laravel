<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaimagenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaimagenes', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('nombreoriginal', 150);
			$table->string('nombre', 50)->unique('nombre');
			$table->smallInteger('ancho');
			$table->smallInteger('alto');
			$table->char('tipo', 15);
			$table->binary('imagen', 16777215);
			$table->integer('tamano');
			$table->smallInteger('id_tema');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaimagenes');
	}

}
