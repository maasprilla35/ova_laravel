<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvaarchivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovaarchivos', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('nombreoriginal', 150);
			$table->string('nombre', 50)->unique('nombre');
			$table->string('tipo', 6);
			$table->integer('tamano');
			$table->binary('archivo', 16777215);
			$table->smallInteger('id_tema');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovaarchivos');
	}

}
