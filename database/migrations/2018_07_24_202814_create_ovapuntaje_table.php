<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvapuntajeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovapuntaje', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->smallInteger('id_usuario');
			$table->string('tema');
			$table->string('areac');
			$table->string('preguntas', 2048);
			$table->smallInteger('npreguntas');
			$table->string('fecha', 10);
			$table->string('hora', 8);
			$table->smallInteger('repeticiones');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovapuntaje');
	}

}
