<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOvatemasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ovatemas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('titulo', 150);
			$table->text('introduccion');
			$table->text('prerequisitos', 65535);
			$table->integer('status');
			$table->integer('id_areac');
			$table->text('ayuda');
			$table->smallInteger('orden');
			$table->string('titulovideo', 100);
			$table->string('enlacevideo');
			$table->string('tituloaudio', 100);
			$table->string('archivoaudio');
			$table->string('tituloexterno', 100);
			$table->string('contenidoexterno');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ovatemas');
	}

}
