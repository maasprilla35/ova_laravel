<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', array('as' => 'welcome', 'uses' => 'Admin\HomeController@welcome' ));


Route::get('ovas/', array('as' => 'ovasView', 'uses' => 'Admin\OvasController@index' ));
Route::get('login/', array('as' => 'goLogin', 'uses' => 'Admin\HomeController@goLogin' ));
Route::post('inicioSesion/', array('as' => 'inicioSesion', 'uses' => 'Admin\HomeController@login' ));


Route::get('areadeusuario/', array('as' => 'areadeusuario', 'uses' => 'Admin\UsuarioController@index' ));
Route::get('usuarios/', array('as' => 'adminusuarios', 'uses' => 'Admin\UsuarioController@admin' ));
Route::get('usuarios/create', array('as' => 'createusuarios', 'uses' => 'Admin\UsuarioController@create' ));
Route::post('usuarios/store', array('as' => 'storesuarios', 'uses' => 'Admin\UsuarioController@store' ));
Route::get('usuarios/edit/{id}', array('as' => 'goEditUsuarios', 'uses' => 'Admin\UsuarioController@goEditUsuarios' ));
Route::post('usuarios/update', array('as' => 'editarUsuarios', 'uses' => 'Admin\UsuarioController@editarUsuarios' ));
Route::post('usuarios/filtroTipo', array('as' => 'usuarioFiltroTipo', 'uses' => 'Admin\UsuarioController@usuarioFiltroTipo' ));
Route::post('usuarios/filtro', array('as' => 'usuarioFiltro', 'uses' => 'Admin\UsuarioController@usuarioFiltro' ));


Route::get('asignaturas/edit/{id}', array('as' => 'goAsignaturasUpdate', 'uses' => 'Admin\AsignaturaController@goAsignaturasUpdate' ));
Route::get('asignaturas/', array('as' => 'adminasignaturas', 'uses' => 'Admin\AsignaturaController@index' ));
Route::post('asignaturas/update', array('as' => 'adminasignaturasupdate', 'uses' => 'Admin\AsignaturaController@edit' ));

Route::resource('pages/areasconocimientos','Admin\AreaConocimientoController');
Route::get('areasconocimientos/', array('as' => 'adminareasconocimientos', 'uses' => 'Admin\AreaConocimientoController@index' ));
Route::get('areasconocimientos/create', array('as' => 'goCrearAreas', 'uses' => 'Admin\AreaConocimientoController@goCrearAreas' ));
Route::post('areasconocimientos/store', array('as' => 'crearAreas', 'uses' => 'Admin\AreaConocimientoController@crearAreas' ));
Route::get('areasconocimientos/edit/{id}', array('as' => 'goEditAreas', 'uses' => 'Admin\AreaConocimientoController@goEditAreas' ));
Route::post('areasconocimientos/update', array('as' => 'editarAreas', 'uses' => 'Admin\AreaConocimientoController@editarAreas' ));
Route::get('areasconocimientos/cambiarestado/{id}', array('as' => 'areccambiarestado', 'uses' => 'Admin\AreaConocimientoController@cambiarestado' ));


Route::resource('pages/temas','Admin\TemaController');
Route::get('temas/', array('as' => 'admintemas', 'uses' => 'Admin\TemaController@index' ));
Route::get('temas/ver/{id}', array('as' => 'temasver', 'uses' => 'Admin\TemaController@temasver' ));
Route::get('temas/create', array('as' => 'goTemas', 'uses' => 'Admin\TemaController@goTemas' ));
Route::get('temas/areas/{id}', array('as' => 'temasova', 'uses' => 'Admin\TemaController@temasova' ));
Route::get('administrartemas/areas/{id}', array('as' => 'administrartemas', 'uses' => 'Admin\TemaController@show' ));
Route::post('temas/store', array('as' => 'crearTemas', 'uses' => 'Admin\TemaController@crearTemas' ));
Route::get('temas/edit/{id}', array('as' => 'goEditTemas', 'uses' => 'Admin\TemaController@goEditTemas' ));
Route::post('temas/update', array('as' => 'editarTemas', 'uses' => 'Admin\TemaController@editarTemas' ));
Route::get('temas/editAyuda/{id}', array('as' => 'goEditAyuda', 'uses' => 'Admin\TemaController@goEditAyuda' ));
Route::post('temas/updateAyuda', array('as' => 'editarAyuda', 'uses' => 'Admin\TemaController@editarAyuda' ));
Route::get('temas/enlaces/{id}', array('as' => 'goEnlaces', 'uses' => 'Admin\TemaController@goEnlaces' ));
Route::get('temas/crearEnlace/{id}', array('as' => 'irCreateEnlace', 'uses' => 'Admin\TemaController@irCreateEnlace' ));
Route::post('temas/crearURl', array('as' => 'createEnlace', 'uses' => 'Admin\TemaController@createEnlace' ));
Route::get('temas/irEditarURl/{id}', array('as' => 'irEditarURl', 'uses' => 'Admin\TemaController@irEditarURl' ));
Route::post('temas/editarURl', array('as' => 'editarURl', 'uses' => 'Admin\TemaController@editarURl' ));
Route::get('temas/cambiarestado/{id}', array('as' => 'temacambiarestado', 'uses' => 'Admin\TemaController@cambiarestado' ));
Route::get('temas/area/cambiarestado/{id}', array('as' => 'temaareacambiarestado', 'uses' => 'Admin\TemaController@cambiarestadobyArea' ));
Route::get('temas/irCrearVideo/{id}', array('as' => 'irCrearVideo', 'uses' => 'Admin\TemaController@irCrearVideo' ));
Route::post('temas/crearVideo', array('as' => 'createVideo', 'uses' => 'Admin\TemaController@createVideo' ));
Route::get('temas/irCrearAudio/{id}', array('as' => 'irCrearAudio', 'uses' => 'Admin\TemaController@irCrearAudio' ));
Route::get('temas/irCrearExterno/{id}', array('as' => 'irCrearExterno', 'uses' => 'Admin\TemaController@irCrearExterno' ));
Route::post('temas/crearExterno', array('as' => 'crearExterno', 'uses' => 'Admin\TemaController@crearExterno' ));
Route::get('temas/actividad/{id}', array('as' => 'goActividad', 'uses' => 'Admin\TemaController@goActividad' ));
Route::get('temas/crearActividad/{id}', array('as' => 'goCrearActividad', 'uses' => 'Admin\TemaController@goCrearActividad' ));
Route::post('temas/storeActividad', array('as' => 'storeActividad', 'uses' => 'Admin\TemaController@storeActividad' ));
Route::get('temas/editaractividad/{idtema}/{id}', array('as' => 'goActividadEditar', 'uses' => 'Admin\TemaController@goActividadEditar' ));
Route::post('temas/editActividad', array('as' => 'editActividad', 'uses' => 'Admin\TemaController@editActividad' ));


Route::get('temas/evaluacion/{id}', array('as' => 'goEvaluacion', 'uses' => 'Admin\TemaController@goEvaluacion' ));
Route::get('temas/crearEvaluacion/{id}', array('as' => 'goCrearEvaluacion', 'uses' => 'Admin\TemaController@goCrearEvaluacion' ));
Route::get('temas/cambiarEstadoEvaluacion/{id}', array('as' => 'cambiarEstadoEvaluacion', 'uses' => 'Admin\TemaController@cambiarEstadoEvaluacion' ));
Route::get('temas/cambiarEstadoActividad/{id}', array('as' => 'cambiarEstadoActividad', 'uses' => 'Admin\TemaController@cambiarEstadoActividad' ));
Route::post('temas/storeEvaluacion', array('as' => 'storeEvaluacion', 'uses' => 'Admin\TemaController@storeEvaluacion' ));
Route::get('temas/editarevaluacion/{idtema}/{id}', array('as' => 'goEvaluacionEditar', 'uses' => 'Admin\TemaController@goEvaluacionEditar' ));
Route::post('temas/editEvaluacion', array('as' => 'editEvaluacion', 'uses' => 'Admin\TemaController@editEvaluacion' ));

Route::post('temas/deleteUrl/{id}', array('as' => 'deleteUrl', 'uses' => 'Admin\TemaController@deleteUrl' ));
//Route::delete('temas/destroy', array('as' =>'deleteTemas', 'uses' => 'Admin\TemaController@destroy' ));
Route::post('temas/filtroTipo', array('as' => 'temasFiltroTipo', 'uses' => 'Admin\TemaController@temaFiltroTipo' ));
Route::post('temas/filtro', array('as' => 'temasFiltro', 'uses' => 'Admin\TemaController@temaFiltro' ));
Route::post('temas/deleteActividad/{id}', array('as' => 'deleteActividad', 'uses' => 'Admin\TemaController@deleteActividad' ));
Route::post('temas/deleteEvaluacion/{id}', array('as' => 'deleteEvaluacion', 'uses' => 'Admin\TemaController@deleteEvaluacion' ));
Route::get('temas/realizarevaluacion/{id}', array('as' => 'evaluaciontemas', 'uses' => 'Admin\TemaController@evaluaciontemas' ));
Route::post('temas/resultadosTemas', array('as' => 'resultadosTemas', 'uses' => 'Admin\TemaController@resultadosTemas' ));



Route::resource('pages/subtemas','Admin\SubtemaController');
Route::get('subtemas/{id}', array('as' => 'adminsubtemas', 'uses' => 'Admin\SubtemaController@index' ));
Route::get('subtemas/create/{id}', array('as' => 'goSubtemas', 'uses' => 'Admin\SubtemaController@goSubtemas' ));
Route::post('subtemas/store', array('as' => 'crearSubtemas', 'uses' => 'Admin\SubtemaController@crearSubtemas' ));
Route::get('subtemas/edit/{id}', array('as' => 'goEditSubtemas', 'uses' => 'Admin\SubtemaController@goEditSubtemas' ));
Route::post('subtemas/update', array('as' => 'editarSubtemas', 'uses' => 'Admin\SubtemaController@editarSubtemas' ));
//Route::delete('temas/destroy', array('as' => 'deleteTemas', 'uses' => 'Admin\TemaController@destroy' ));

Route::resource('pages/terminos','Admin\TerminosController');
Route::get('glosarios/', array('as' => 'adminglosario', 'uses' => 'Admin\TerminosController@index' ));
Route::get('glosarios/create', array('as' => 'goGlosarios', 'uses' => 'Admin\TerminosController@goGlosarios' ));
Route::post('glosarios/store', array('as' => 'crearGlosarios', 'uses' => 'Admin\TerminosController@crearGlosarios' ));
Route::get('glosarios/edit/{id}', array('as' => 'goEditGlosarios', 'uses' => 'Admin\TerminosController@goEditGlosarios' ));
Route::post('glosarios/update', array('as' => 'editarGlosarios', 'uses' => 'Admin\TerminosController@editarGlosarios' ));

Route::resource('pages/referencias','Admin\ReferenciaController');
Route::get('bibliografias/', array('as' => 'adminbibliografia', 'uses' => 'Admin\ReferenciaController@index' ));
Route::get('bibliografias/create', array('as' => 'goBibliografias', 'uses' => 'Admin\ReferenciaController@goBibliografias' ));
Route::post('bibliografias/store', array('as' => 'crearBibliografias', 'uses' => 'Admin\ReferenciaController@crearBibliografias' ));
Route::get('bibliografias/edit/{id}', array('as' => 'goEditBibliografias', 'uses' => 'Admin\ReferenciaController@goEditBibliografias' ));
Route::post('bibliografias/update', array('as' => 'editarBibliografias', 'uses' => 'Admin\ReferenciaController@editarBibliografias' ));

Route::get('colores/', array('as' => 'admincolores', 'uses' => 'Admin\ConfiguracionController@colores' ));
Route::get('administrardiseno/', array('as' => 'administrardiseno', 'uses' => 'Admin\ConfiguracionController@administrardiseno' ));
Route::get('modificardiseno/', array('as' => 'modificardiseno', 'uses' => 'Admin\ConfiguracionController@modificardiseno' ));
Route::post('modificardiseno/update', array('as' => 'modificardisenoupdate', 'uses' => 'Admin\ConfiguracionController@modificardisenoupdate' ));

Route::post('uploadfile/', array('as' => 'uploadfile', 'uses' => 'Admin\ConfiguracionController@uploadfile' ));

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
