<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enlace extends Model
{
  protected $table='ovaenlaces';
  protected $fillable=[
    'id',
    'titulo',
    'url',
    'id_tema'
  ];
  public $timestamps = false;


  protected $guarded=[

  ];
}
