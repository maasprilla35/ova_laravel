<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
  protected $table='ovatiposactividades';
  protected $fillable=[
    'id',
    'tipo'
  ];
  public $timestamps = false;
  

  protected $guarded=[

  ];
}
