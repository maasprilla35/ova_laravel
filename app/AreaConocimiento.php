<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaConocimiento extends Model
{
  protected $table='ovaareasc';
  protected $fillable=[
    'id',
    'titulo',
    'status',
    'orden'
  ];
  public $timestamps = false;

  protected $guarded=[

  ];
}
