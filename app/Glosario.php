<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glosario extends Model
{
  protected $table='ovaglosario';
  protected $fillable=[
    'id',
    'termino',
    'definicion'
  ];
  public $timestamps = false;


  protected $guarded=[

  ];
}
