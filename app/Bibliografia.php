<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bibliografia extends Model
{
  protected $table='ovabibliografia';
  protected $fillable=[
    'id',
    'titulo',
    'autor',
    'info',
    'id_tema'
  ];
  public $timestamps = false;


  protected $guarded=[

  ];
}
