<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Diseno extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ovadiseno';
    protected $fillable = [
        'numopcfilas', 'colorprincipal', 'colorsecundario','colorterciario','colorderesalte','colordeletraresalte'
    ];
    public $timestamps = false;


}

