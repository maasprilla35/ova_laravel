<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtema extends Model
{
  protected $table='ovatemascontenidos';
  protected $fillable=[
    'id',
    'titulo',
    'contenido',
    'orden',
    'id_tema',
    'status'
  ];
  public $timestamps = false;


  protected $guarded=[

  ];
}
