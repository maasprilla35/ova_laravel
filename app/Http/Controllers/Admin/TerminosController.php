<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Glosario;

class TerminosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $glosario = new Glosario();
      $glosarios = $glosario->all();

      return view('pages/terminos/admin-terminos')->with('glosarios', $glosarios);
    }

    public function goGlosarios()
    {
      return view('pages/terminos/crearTerminos');
    }

    public function crearGlosarios(Request $request)
    {
        $glosario = new Glosario();
        $glosario->termino = $request->termino;
        $glosario->definicion = $request->definicion;
        $glosario->save();
        //return view('pages/usuarios/admin-usuarios');
        Session::flash('alert-success', 'Glosario creado exitosamente!');
        return redirect()->route('adminglosario')->with('success','Registro creado satisfactoriamente');
        //return response()->json($areas);
    }

    public function goEditGlosarios($id)
    {
        $glosario = Glosario::find($id);
        return view('pages/terminos/modificarTermino')->with('glosario',$glosario);
    }


    public function editarGlosarios(Request $request){
        $glosario = Glosario::find($request->id);
        $glosario->termino=$request->get('termino');
        $glosario->definicion=$request->get('definicion');
        $glosario->update();
        Session::flash('alert-success', 'Glosario editado exitosamente!');
        return redirect()->route('adminglosario')->with('success','Registro creado satisfactoriamente');
        //return response()->json($request->id);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    public function editUser()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    public function destroy($id){

        $glosario = Glosario::find($id);
        $glosario->delete();
        //return response()->json($tema);
        Session::flash('alert-success', 'Glosario eliminado exitosamente!');
        return redirect()->route('adminglosario')->with('success','Registro creado satisfactoriamente');
    }
}
