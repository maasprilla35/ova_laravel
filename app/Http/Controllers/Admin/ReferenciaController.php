<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Bibliografia;

class ReferenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $biblio = new Bibliografia();
        $biblios = $biblio->all();

        return View::make('pages/referencias/admin-referencias')->with('biblios', $biblios);
        
    }

    public function goBibliografias()
    {
      return view('pages/referencias/crearReferencia');
    }

    public function crearBibliografias(Request $request)
    {
        $biblio = new Bibliografia();
        $biblio->titulo = $request->titulo;
        $biblio->autor = $request->autor;
        $biblio->info = $request->info;
        $biblio->id_tema = 0;
        $biblio->save();
        //return view('pages/usuarios/admin-usuarios');
        Session::flash('alert-success', 'Bibliografia creado exitosamente!');
        return redirect()->route('adminbibliografia')->with('success','Registro creado satisfactoriamente');
        //return response()->json($areas);
    }

    public function goEditBibliografias($id)
    {        
        $biblio = Bibliografia::find($id);
        return view('pages/referencias/editarReferencia')->with('biblio',$biblio);
    }


    public function editarBibliografias(Request $request){
        $biblio = Bibliografia::find($request->id);
        $biblio->titulo=$request->get('titulo');
        $biblio->autor=$request->get('autor');
        $biblio->info=$request->get('info');
        $biblio->update();
        Session::flash('alert-success', 'Bibliografia editada exitosamente!');
        return redirect()->route('adminbibliografia')->with('success','Registro creado satisfactoriamente');
        //return response()->json($request->id);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }


    public function editUser()
    {
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    public function destroy($id){
    
        $biblio = Bibliografia::find($id);
        $biblio->delete();
        //return response()->json($tema);
        Session::flash('alert-success', 'Bibliografia eliminada exitosamente!');
        return redirect()->route('adminbibliografia')->with('success','Registro creado satisfactoriamente');
    }
}
