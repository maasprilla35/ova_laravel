<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Diseno;
class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {  
    }

    public function colores()
    {   
        $diseno = new Diseno();
        $diseno = $diseno->find(1);
        return View::make('pages/configuraciones/colores')->with('diseno',$diseno);
    }

    public function administrardiseno(){
        $diseno = new Diseno();
        $diseno = $diseno->find(1);
        return View::make('pages/configuraciones/diseno')->with('diseno',$diseno);
    }

    public function modificardiseno(){
        $diseno = new Diseno();
        $diseno = $diseno->find(1);
        return View::make('pages/configuraciones/modificardiseno')->with('diseno',$diseno);
    }

    public function modificardisenoupdate(Request $request){
        $diseno = new Diseno();
        $diseno = $diseno->find(1);
        $diseno->colorprincipal = $request->colorprincipal;
        $diseno->colorsecundario = $request->colorsecundario;
        $diseno->save();
        //return View::make('pages/configuraciones/diseno')->with('diseno',$diseno);
        return redirect()->route('administrardiseno', ['diseno',$diseno])->with('success','Registro creado satisfactoriamente');

    }

    public function uploadfile(Request $request){
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $name = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/images/cursos');

            $image->move($destinationPath, $name);


            return response()->json(['location' => 'http://'.$_SERVER['HTTP_HOST'].'/images/cursos/'.$name]);


          }
    
        return response()->json($request);
        return response()->json(['location' => 'imagenes3.png']);
    }
    



}
