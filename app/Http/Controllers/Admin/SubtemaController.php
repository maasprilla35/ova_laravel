<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Tema;
use App\Subtema;

class SubtemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $subtema = DB::table('ovatemascontenidos')
        ->join('ovatemas','ovatemascontenidos.id_tema','=','ovatemas.id')
        ->select('ovatemascontenidos.*', 'ovatemas.titulo as tituloArea')
        ->where('ovatemas.id', '=', $request->id)
        ->get();

        $tema = Tema::find($request->id);

        return View::make('pages/subtemas/admin-subtemas')->with('subtema', $subtema)->with('tema', $tema);
    }

    public function goSubtemas($id)
    {
        $tema = Tema::find($id);

        $subtema = new Subtema();
        $subtema = $subtema->all();
        return View::make('pages/subtemas/crearSubtema')->with('tema',$tema)->with('subtema',$subtema);
    }


    public function crearSubtemas(Request $request)
    {   
        $tema = Tema::find($request->id);
        $subtema = new Subtema();
        $subtema->titulo = $request->titulo;
        $subtema->contenido = $request->contenido;
        $subtema->status = 1;
        $subtema->id_tema  = $request->id;
        $subtema->save();
        return redirect()->route('adminsubtemas', ['id' => $request->id])->with('success','Registro creado satisfactoriamente');

    }

    public function goEditSubtemas($id)
    {
        $subtema = Subtema::find($id);
        return view('pages/subtemas/modificarSubtema')->with('subtema',$subtema);

    }


    public function editarSubtemas(Request $request){
        $subtema = Subtema::find($request->id);
        $subtema->titulo=$request->get('titulo');
        $subtema->contenido=$request->get('contenido');
        $subtema->status=$request->get('status');
        $subtema->update();
        return redirect()->route('adminsubtemas', ['id' => $request->id_tema])->with('success','Registro creado satisfactoriamente');
        //return response()->json($request->id);
    }   

    public function destroy(Request $request){

        $subtema = Subtema::find($request->id);
        $subtema->delete();
        //return response()->json($tema);
        return redirect()->route('adminsubtemas', ['id' => $subtema->id_tema])->with('success','Registro creado satisfactoriamente');
        //return View::make('pages/subtemas/admin-subtemas/' . $subtema->id_tema);
    }
}
