<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\AreaConocimiento;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
  
      //return view('pages/areasconocimientos/ovas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function goLogin()
     {
   
       return view('pages/configuraciones/login');
     }

     public function welcome()
     {
        $temas = DB::table('ovatemas')
        ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
        ->select(DB::raw('DISTINCT ovatemas.*'))
        ->get();
       return view('welcome')->with('temas', $temas );
     }

    public function login(Request $request)
    {

        $login = Auth::attempt(['nick' => $request->nick, 'password' => $request->password]);

         if($login){
             Session::put('idUser',  Auth::id());
             return redirect()->route('areadeusuario')->with('users',Auth::user())->with('success','Registro creado satisfactoriamente');

         }else{
             ///return redirect()->route('goLogin', ['alert', 'Usuario o Contraseña Incorrecta!']);
             Session::flash('alert-warning', 'Usuario o Contraseña Incorrecta!');
             return View::make('pages/configuraciones/login');

         }
    }


    public function editUser()
    {
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy()
     {
       
     }
}
