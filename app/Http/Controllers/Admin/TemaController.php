<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\AreaConocimiento;
use App\Tema;
use App\Subtema;
use App\Enlace;
use App\Actividad;
use App\Seleccion;
use App\Models\Diseno;

class TemaController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function index()
  {
    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
    ->get();

    $area = new AreaConocimiento();
    $areas = $area->all();

    $subtema = new Subtema();
    $subtemas = $subtema->all();

    return View::make('pages/temas/admin-temas')->with('temas', $temas)->with('subtemas', $subtemas)->with('areas', $areas )->with('valuefilter', 0 );

    // return view('pages/temas/admin-temas');
  }

  public function goTemas()
  {
    $area = new AreaConocimiento();
    $areas = $area->all();
    return view('pages/temas/crearTemas')->with('areas', $areas );
  }

  public function temasova(Request $request)
  {
    //$temas = new Tema();
    //$temas = $temas->where('id_areac', $request->id)->get();
    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->join('ovatemascontenidos','ovatemas.id','=','ovatemascontenidos.id_tema')
    ->where('ovatemas.id_areac', $request->id)
    ->select(DB::raw('DISTINCT ovatemas.*'))
    ->get();
    return view('pages/temas/temasova')->with('temas', $temas );
  }

  public function show(Request $request)
  {
    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->where('ovatemas.id_areac', $request->id)
    ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
    ->get();

    return view('pages/temas/admin-temas-areas')->with('temas', $temas );
  }


  public function crearTemas(Request $request)
  {
    $tema = new Tema();
    $tema->titulo = $request->titulo;
    $tema->introduccion = $request->introduccion;
    $tema->prerequisitos = $request->prerequisitos;
    $tema->status = 1;
    $tema->id_areac = $request->id_areac;
    $tema->save();
    //return view('pages/usuarios/admin-usuarios');
    Session::flash('alert-success', 'Tema creado exitosamente!');
    return redirect()->route('admintemas')->with('success','Registro creado satisfactoriamente');
    //return response()->json($areas);
  }

  public function goEditTemas($id)
  {
    $tema = Tema::find($id);
    $area = new AreaConocimiento();
    $areas = $area->all();
    return view('pages/temas/modificarTema')->with('tema',$tema)->with('areas', $areas );
  }


  public function editarTemas(Request $request){
    $tema = Tema::find($request->id);
    $tema->titulo=$request->get('titulo');
    $tema->introduccion=$request->get('introduccion');
    $tema->prerequisitos=$request->get('prerequisitos');
    $tema->status=$request->get('status');
    $tema->id_areac=$request->get('id_areac');
    $tema->update();
    Session::flash('alert-success', 'Tema editado exitosamente!');
    return redirect()->route('admintemas')->with('success','Registro creado satisfactoriamente');
    //return response()->json($request->id);
  }

  public function goEditAyuda($id)
  {
    $tema = Tema::find($id);
    return view('pages/temas/modificarAyuda')->with('tema',$tema);
  }


  public function editarAyuda(Request $request){
    $tema = Tema::find($request->id);
    $tema->ayuda=$request->get('ayuda');
    $tema->update();
    Session::flash('alert-success', 'Ayuda actualizada exitosamente!');
    return redirect()->route('admintemas')->with('success','Registro creado satisfactoriamente');
    //return response()->json($request->id);
  }

  public function goEnlaces(Request $request)
  {
    $enlace = DB::table('ovaenlaces')
    ->join('ovatemas','ovaenlaces.id_tema','=','ovatemas.id')
    ->where('ovaenlaces.id_tema', $request->id)
    ->select('ovaenlaces.*', 'ovatemas.titulo as tituloTema')
    ->get();

    $tema = Tema::find($request->id);
    return view('pages/temas/enlaces')->with('tema',$tema)->with('enlace',$enlace);
  }


  public function irCreateEnlace(Request $request)
  {
    $tema = Tema::find($request->id);
    return View::make('pages/temas/crearEnlace')->with('tema',$tema);
  }

  public function createEnlace(Request $request)
  {
    $tema = Tema::find($request->id);

    $enlace = new Enlace();
    $enlace->titulo = $request->titulo;
    $enlace->url = $request->url;
    $enlace->id_tema = $request->id;
    $enlace->save();
    Session::flash('alert-success', 'Enlace creado exitosamente!');
    return redirect()->route('goEnlaces',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }


  public function irEditarURl(Request $request)
  {
    $enlace = new Enlace();
    $enlace = $enlace->find($request->id);
    return View::make('pages/temas/modificarEnlace')->with('enlace',$enlace);
  }

  public function editarURl(Request $request){

    $enlace = Enlace::find($request->id);
    $enlace->titulo=$request->get('titulo');
    $enlace->url=$request->get('url');
    $enlace->update();
    Session::flash('alert-success', 'Enlace editado exitosamente!');
    return redirect()->route('goEnlaces',['id'=> $enlace->id_tema])->with('success','Registro creado satisfactoriamente');

  }

  public function deleteUrl(Request $request){
    $enlace = Enlace::find($request->id);
    $enlace->delete();
    Session::flash('alert-success', 'Enlace eliminado exitosamente!');
    return redirect()->route('goEnlaces',['id'=> $enlace->id_tema])->with('success','Registro creado satisfactoriamente');
  }


  public function destroy($id){

    $tema = Tema::find($id);
    $tema->delete();
    Session::flash('alert-success', 'Tema eliminado exitosamente!');
    return redirect()->route('admintemas')->with('success','Registro creado satisfactoriamente');
  }
  
  public function cambiarestado(Request $request){
    $tema = Tema::find($request->id);
    if($tema->status==0){
      $tema->status=1;
    }else{
      $tema->status=0;
    }
    $tema->update();
    return redirect()->route('admintemas')->with('success','Registro creado satisfactoriamente');
  }

  public function cambiarestadobyArea(Request $request){
    $tema = Tema::find($request->id);
    if($tema->status==0){
      $tema->status=1;
    }else{
      $tema->status=0;
    }
    $tema->update();
    return redirect()->route('administrartemas',['id'=> $tema->id_areac])->with('success','Registro creado satisfactoriamente');
  }

  public function irCrearVideo(Request $request)
  {
    $tema = Tema::find($request->id);
    return View::make('pages/temas/video')->with('tema',$tema);
  }

  public function createVideo(Request $request)
  {
    $tema = Tema::find($request->id);
    $tema->titulovideo = $request->titulovideo;
    $tema->enlacevideo = $request->enlacevideo;
    $tema->id = $request->id;
    //return response()->json($tema . "video");
    $tema->update();
    Session::flash('alert-success', 'video creado exitosamente!');
    return redirect()->route('goEnlaces',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }



  public function irCrearAudio(Request $request)
  {
    $tema = Tema::find($request->id);
    return View::make('pages/temas/video')->with('tema',$tema);
  }

  public function irCrearExterno(Request $request)
  {
    $tema = Tema::find($request->id);
    return View::make('pages/temas/externo')->with('tema',$tema);
  }

  public function crearExterno(Request $request)
  {
      $tema = Tema::find($request->id);
      $tema->tituloexterno = $request->tituloexterno;
      $tema->contenidoexterno = $request->contenidoexterno;
      $tema->id = $request->id;
    //  return response()->json($tema . "video");
      $tema->update();
      Session::flash('alert-success', 'Externo creado exitosamente!');
    return redirect()->route('goEnlaces',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }

  public function goActividad(Request $request)
  {
    $secc = DB::table('ovaseleccion')
    ->join('ovatiposactividades','ovaseleccion.tipo','=','ovatiposactividades.id')
    ->where([
      ['ovaseleccion.id_tema', '=', $request->id],
      ['ovaseleccion.categoria', '=', 1],
    ])
    ->select('ovaseleccion.*', 'ovatiposactividades.tipo as nombActividad')
    ->get();
    // $secc = new Seleccion();
    // $secc = $secc->all();

    $tema = Tema::find($request->id);
    return View::make('pages/temas/actividad')->with('tema',$tema)->with('secc', $secc);
  }

  public function goCrearActividad(Request $request)
  {
    $actividad = new Actividad();
    $actividad = $actividad->all();

    $tema = Tema::find($request->id);
    return View::make('pages/temas/crearActividad')->with('tema',$tema)->with('actividad',$actividad);
  }

  public function storeActividad(Request $request)
  {
    $tema = Tema::find($request->id);

    if ($request->tipo == 1) {
      if ($request->numopciones == 3) {
        $seleccion = new Seleccion();
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcioncorrecta = $request->opcioncorrecta3o;
        $seleccion->tipo = $request->tipo;
        $seleccion->status = 1;
        $seleccion->id_tema = $tema->id;
      }elseif ($request->numopciones == 4) {
        $seleccion = new Seleccion();
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcion4 = $request->opcion4;
        $seleccion->opcioncorrecta = $request->opcioncorrecta4o;
        $seleccion->tipo = $request->tipo;
        $seleccion->status = 1;
        $seleccion->id_tema = $tema->id;
      }
    }elseif ( $request->tipo == 2){
      $seleccion = new Seleccion();
      $seleccion->tipo = $request->tipo;
      $seleccion->enunciado = $request->enunciado;
      $seleccion->opcioncorrecta = $request->opcioncorrectavof;
      $seleccion->status = 1;
      $seleccion->numopciones = 2;
      $seleccion->id_tema = $tema->id;
    }
    $seleccion->categoria = 1;
    $seleccion->save();
    Session::flash('alert-success', 'Actividad creada exitosamente!');
    return redirect()->route('goActividad',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }


  public function deleteActividad(Request $request){
    $actividad = Seleccion::find($request->id);
    $actividad->delete();
    Session::flash('alert-success', 'Actividad eliminado exitosamente!');
    return redirect()->route('goActividad',['id'=> $actividad->id_tema])->with('success','Registro creado satisfactoriamente');
  }


  public function goEvaluacion(Request $request)
  {
    $secc = DB::table('ovaseleccion')
    ->join('ovatiposactividades','ovaseleccion.tipo','=','ovatiposactividades.id')
    ->where([
      ['ovaseleccion.id_tema', '=', $request->id],
      ['ovaseleccion.categoria', '=', 2],
    ])
    ->select('ovaseleccion.*', 'ovatiposactividades.tipo as nombActividad')
    ->get();
    // $secc = new Seleccion();
    // $secc = $secc->all();

    $tema = Tema::find($request->id);
    return View::make('pages/temas/evaluacion')->with('tema',$tema)->with('secc', $secc);
  }


  public function goCrearEvaluacion(Request $request)
  {
    $actividad = new Actividad();
    $actividad = $actividad->all();

    $tema = Tema::find($request->id);
    return View::make('pages/temas/crearEvaluacion')->with('tema',$tema)->with('actividad',$actividad);
  }

  public function goEvaluacionEditar(Request $request)
  {
    $actividad = new Actividad();
    $actividad = $actividad->all();

    $tema = Tema::find($request->idtema);

    $seleccion= Seleccion::find($request->id);

    return View::make('pages/temas/editarEvaluacion')->with('tema',$tema)->with('actividad',$actividad)->with('seleccion',$seleccion);
  }

  public function storeEvaluacion(Request $request)
  {
    $tema = Tema::find($request->id);

    if ( $request->tipo == 1) {
      if ($request->numopciones == 3) {
        $seleccion = new Seleccion();
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->valor = $request->valor;
        $seleccion->opcioncorrecta = $request->opcioncorrecta3o;
        $seleccion->tipo = $request->tipo;
        $seleccion->status = 1;
        $seleccion->id_tema = $tema->id;
      }elseif ($request->numopciones == 4) {
        $seleccion = new Seleccion();
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcion4 = $request->opcion4;
        $seleccion->valor = $request->valor;
        $seleccion->opcioncorrecta = $request->opcioncorrecta4o;
        $seleccion->tipo = $request->tipo;
        $seleccion->status = 1;
        $seleccion->id_tema = $tema->id;
      }
    }elseif ( $request->tipo == 2){
      $seleccion = new Seleccion();
      $seleccion->tipo = $request->tipo;
      $seleccion->enunciado = $request->enunciado;
      $seleccion->valor = $request->valor;
      $seleccion->opcioncorrecta = $request->opcioncorrectavof;
      $seleccion->status = 1;
      $seleccion->numopciones = 2;
      $seleccion->id_tema = $tema->id;
    }
    $seleccion->categoria = 2;
    $seleccion->save();
    Session::flash('alert-success', 'Evalucacion creada exitosamente!');

    return redirect()->route('goEvaluacion',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }

  public function editEvaluacion(Request $request)
  {
    //return response()->json($request->enunciado);
    $tema = Tema::find($request->idtema);
    $seleccion = new Seleccion();
    $seleccion = $seleccion->find($request->id);

    if ($seleccion->tipo == 1) {

      if ($request->numopciones == 3) {

        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->valor = $request->valor;
        $seleccion->opcioncorrecta = $request->opcioncorrecta3o;
        $seleccion->status = 1;
        $seleccion->id_tema = $request->idtema;
      }elseif ($request->numopciones == 4) {
  
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcion4 = $request->opcion4;
        $seleccion->valor = $request->valor;
        $seleccion->opcioncorrecta = $request->opcioncorrecta4o;
        $seleccion->status = 1;
        $seleccion->id_tema = $request->idtema;
      }
    }elseif ( $seleccion->tipo == 2){
      $seleccion->enunciado = $request->enunciado;
      $seleccion->valor = $request->valor;
      $seleccion->opcioncorrecta = $request->opcioncorrectavof;
      $seleccion->status = 1;
      $seleccion->numopciones = null;
      $seleccion->id_tema = $request->idtema;
    }
    $seleccion->update();
    Session::flash('alert-success', 'Evalucacion editada exitosamente!');

    return redirect()->route('goEvaluacion',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }

  public function editActividad(Request $request)
  {
    //return response()->json($request->enunciado);
    $tema = Tema::find($request->idtema);
    $seleccion = new Seleccion();
    $seleccion = $seleccion->find($request->id);

    if ($seleccion->tipo == 1) {

      if ($request->numopciones == 3) {

        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcioncorrecta = $request->opcioncorrecta3o;
        $seleccion->status = 1;
        $seleccion->id_tema = $request->idtema;
      }elseif ($request->numopciones == 4) {
  
        $seleccion->enunciado = $request->enunciado;
        $seleccion->numopciones = $request->numopciones;
        $seleccion->opcion1 = $request->opcion1;
        $seleccion->opcion2 = $request->opcion2;
        $seleccion->opcion3 = $request->opcion3;
        $seleccion->opcion4 = $request->opcion4;
        $seleccion->opcioncorrecta = $request->opcioncorrecta4o;
        $seleccion->status = 1;
        $seleccion->id_tema = $request->idtema;
      }
    }elseif ( $seleccion->tipo == 2){
      $seleccion->enunciado = $request->enunciado;
      $seleccion->opcioncorrecta = $request->opcioncorrectavof;
      $seleccion->status = 1;
      $seleccion->numopciones = null;
      $seleccion->id_tema = $request->idtema;
    }
    $seleccion->update();
    Session::flash('alert-success', 'Evalucacion editada exitosamente!');

    return redirect()->route('goActividad',['id'=> $tema->id])->with('success','Registro creado satisfactoriamente');
  }

  

  public function goActividadEditar(Request $request)
  {
    $actividad = new Actividad();
    $actividad = $actividad->all();

    $tema = Tema::find($request->idtema);

    $seleccion= Seleccion::find($request->id);

    return View::make('pages/temas/editarActividad')->with('tema',$tema)->with('actividad',$actividad)->with('seleccion',$seleccion);
  }


  public function deleteEvaluacion(Request $request){
    $evaluacion = Seleccion::find($request->id);
    $evaluacion->delete();
    Session::flash('alert-success', 'Evaluacion eliminado exitosamente!');
    return redirect()->route('goEvaluacion',['id'=> $evaluacion->id_tema])->with('success','Registro creado satisfactoriamente');
  }

  public function cambiarEstadoEvaluacion(Request $request){
    $evaluacion = Seleccion::find($request->id);
    if($evaluacion->status==0){
      $evaluacion->status=1;
    }else{
      $evaluacion->status=0;
    }
    $evaluacion->update();
    return redirect()->route('goEvaluacion',['id'=> $evaluacion->id_tema])->with('success','Registro creado satisfactoriamente');
  }

  public function cambiarEstadoActividad(Request $request){
    $actividad = Seleccion::find($request->id);
    if($actividad->status==0){
      $actividad->status=1;
    }else{
      $actividad->status=0;
    }
    $actividad->update();
    return redirect()->route('goActividad',['id'=> $actividad->id_tema])->with('success','Registro creado satisfactoriamente');
  }

  public function temaFiltroTipo(Request $request)
  {
      if($request->id_areac==0){
          $temas = DB::table('ovatemas')
          ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
          ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
          ->get();
      
          $area = new AreaConocimiento();
          $areas = $area->all();
      
          $subtema = new Subtema();
          $subtemas = $subtema->all();
      
          return View::make('pages/temas/admin-temas')->with('temas', $temas)->with('subtemas', $subtemas)->with('areas', $areas )->with('valuefilter', 0 );

      }else{
          $temas = DB::table('ovatemas')
          ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
          ->where('ovatemas.id_areac', $request->id_areac)
          ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
          ->get();
      
          $area = new AreaConocimiento();
          $areas = $area->all();
      
          $subtema = new Subtema();
          $subtemas = $subtema->all();
      
          return View::make('pages/temas/admin-temas')->with('temas', $temas)->with('subtemas', $subtemas)->with('areas', $areas )->with('valuefilter', $request->id_areac );

      }

      // return view('pages/temas/admin-temas');
  }

  public function temaFiltro(Request $request)
  {
    
        $temas = DB::table('ovatemas')
        ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
        ->orWhere('ovatemas.titulo', 'like', "%$request->termino%")
        ->orWhere('ovaareasc.titulo','like', "%$request->termino%")
        ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
        ->get();

        $area = new AreaConocimiento();
        $areas = $area->all();

        $subtema = new Subtema();
        $subtemas = $subtema->all();

        return View::make('pages/temas/admin-temas')->with('temas', $temas)->with('subtemas', $subtemas)->with('areas', $areas )->with('valuefilter', 0 );

  }

  public function temasver(Request $request)
  {
    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->where('ovatemas.id', $request->id)
    ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
    ->first();

    $subtema = new Subtema();
    $subtemas = $subtema->where('ovatemascontenidos.id_tema', $request->id)->get();
    Session::flash('ayuda-content', 'Evalucacion creada exitosamente!');
    Session::flash('video-tema', 'true');
    Session::flash('enlace-tema', 'true');
    Session::flash('evaluacion-tema', 'true');
    return View::make('pages/temas/vertema')->with('temas', $temas)->with('subtemas', $subtemas);

    // return view('pages/temas/admin-temas');
  }

  public function evaluaciontemas(Request $request)
  {
    $evaluaciones = DB::table('ovaseleccion')
    ->join('ovatiposactividades','ovaseleccion.tipo','=','ovatiposactividades.id')
    ->where([
      ['ovaseleccion.id_tema', '=', $request->id],
      ['ovaseleccion.categoria', '=', 2],
    ])
    ->select('ovaseleccion.*', 'ovatiposactividades.tipo as nombActividad')
    ->get();

    $diseno = new Diseno();
    $diseno = $diseno->find(1);

    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->where('ovatemas.id', $request->id)
    ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
    ->first();
    return View::make('pages/temas/evaluacion-realizar')->with('evaluaciones', $evaluaciones)
    ->with('numevaluaciones', count($evaluaciones))
    ->with('idTema', $request->id)
    ->with('temas', $temas)
    ->with('diseno', $diseno);

    // return view('pages/temas/admin-temas');
  }

  public function resultadosTemas(Request $request)
  {
    $evaluaciones = DB::table('ovaseleccion')
    ->join('ovatiposactividades','ovaseleccion.tipo','=','ovatiposactividades.id')
    ->where([
      ['ovaseleccion.id_tema', '=', $request->id_tema],
      ['ovaseleccion.categoria', '=', 2],
    ])
    ->select('ovaseleccion.*', 'ovatiposactividades.tipo as nombActividad')
    ->get();

    $temas = DB::table('ovatemas')
    ->join('ovaareasc','ovatemas.id_areac','=','ovaareasc.id')
    ->where('ovatemas.id', $request->id_tema)
    ->select('ovatemas.*', 'ovaareasc.titulo as tituloArea')
    ->first();

    $diseno = new Diseno();
    $diseno = $diseno->find(1);
    $rest='';
    $correctas=0;
    $incorrectas=0;
    $sumatoriavalor=0;
    $valoracertado=0;
    $listaRespuestas= array();
    for($i=0;$i<count($evaluaciones);$i++){
      $op='opcion'.($i+1);
      $arr[$i] = $request->input($op);
      $sumatoriavalor=(number_format($sumatoriavalor)*1)+(number_format($evaluaciones[$i]->valor)*1);
      if($evaluaciones[$i]->opcioncorrecta==$request->input($op)){
        $rest.=$op;
        $correctas++;
        $valoracertado=(number_format($valoracertado)*1)+(number_format($evaluaciones[$i]->valor)*1);
      }else{
        $incorrectas++;
      }
    }
     
    return View::make('pages/temas/evaluacion-resultados')->with('evaluaciones', $evaluaciones)
    ->with('numevaluaciones', count($evaluaciones))
    ->with('correctas', $correctas)
    ->with('incorrectas', $incorrectas)
    ->with('sumatoriavalor', $sumatoriavalor)
    ->with('valoracertado', $valoracertado)
    ->with('respuestas', $request)
    ->with('temas', $temas)
    ->with('arr', $arr)
    ->with('diseno', $diseno);
    
    // return view('pages/temas/admin-temas');
  }

  

}


