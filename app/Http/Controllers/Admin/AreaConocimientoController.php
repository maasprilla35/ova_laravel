<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\AreaConocimiento;

class AreaConocimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $area = new AreaConocimiento();
        $areas = $area->all();

        return View::Make('pages/areasconocimientos/admin-areasconocimientos')->with('areas', $areas);
    }


    public function goCrearAreas()
    {

    return view('pages/areasconocimientos/crearArea');
    }

    public function crearAreas(Request $request)
    {
        $ultimo = DB::table('ovaareasc')
        ->select(DB::raw('count(*) as orden'))
        ->get();
        $ultimo = $ultimo[0];

        /* return response()->json($ultimo); */

        $area = new AreaConocimiento();
        $area->titulo = $request->titulo;
        $area->status = 1;
        $area->orden = $ultimo->orden;
        $area->save();
        Session::flash('alert-success', 'Area Conocimiento creado exitosamente!');
        return redirect()->route('adminareasconocimientos')->with('success','Registro creado satisfactoriamente');
    }


    public function goEditAreas($id)
    {
        $area = AreaConocimiento::find($id);
        return view('pages/areasconocimientos/modificarArea')->with('area',$area);

    }


    public function editarAreas(Request $request){
        $area = AreaConocimiento::find($request->id);
        $area->titulo=$request->get('titulo');
        $area->status=$request->get('status');
        $area->update();
        Session::flash('alert-success', 'Area Conocimiento editado exitosamente!');
        return redirect()->route('adminareasconocimientos')->with('success','Registro creado satisfactoriamente');
        //return response()->json($request->id);
    }

    public function cambiarestado(Request $request){
        $area = AreaConocimiento::find($request->id);
        if($area->status==0){
            $area->status=1;
        }else{
            $area->status=0;
        }
        $area->update();
        return redirect()->route('adminareasconocimientos')->with('success','Registro creado satisfactoriamente');

    }

     public function destroy($id){    
        $area = AreaConocimiento::find($id);
        $area->delete();
        Session::flash('alert-success', 'Area Conocimiento eliminado exitosamente!');
        return redirect()->route('adminareasconocimientos')->with('success','Registro creado satisfactoriamente');
    }
}
