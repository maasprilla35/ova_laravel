<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use App\TipoUser;
class UsuarioController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function index()
  {
    $user = new User();
    $users = $user->find(Session::get('idUser'));
    
    return View::Make('pages/usuarios/bienvenidousuario')->with('users', $users);
  }

  public function admin()
  {

    $users = DB::table('ovausuarios')
    ->join('ovatipousuario','ovausuarios.id_tipo','=','ovatipousuario.id')
    ->select('ovausuarios.*', 'ovatipousuario.tipo as nombre_tipo_usuario')
    ->get();

    $tipoUsuario = new TipoUser();
    $tipousuarios = $tipoUsuario->all();

     return View::make('pages/usuarios/admin-usuarios')->with('users', $users)->with('tipousuarios', $tipousuarios )->with('valorfiltro', 0 );
  }


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('pages/usuarios/create-usuarios');
  }

  public function usuarioFiltroTipo(Request $request)
  {

    $tipoUsuario = new TipoUser();
    $tipousuarios = $tipoUsuario->all();

      if($request->id_tipo==0){
        $users = DB::table('ovausuarios')
        ->join('ovatipousuario','ovausuarios.id_tipo','=','ovatipousuario.id')
        ->select('ovausuarios.*', 'ovatipousuario.tipo as nombre_tipo_usuario')
        ->get();

        return View::make('pages/usuarios/admin-usuarios')->with('users', $users)->with('tipousuarios', $tipousuarios )->with('valorfiltro', $request->id_tipo );

      }else{
        $users = DB::table('ovausuarios')
        ->join('ovatipousuario','ovausuarios.id_tipo','=','ovatipousuario.id')
        ->where('ovausuarios.id_tipo', $request->id_tipo)
        ->select('ovausuarios.*', 'ovatipousuario.tipo as nombre_tipo_usuario')
        ->get();

        return View::make('pages/usuarios/admin-usuarios')->with('users', $users)->with('tipousuarios', $tipousuarios )->with('valorfiltro', $request->id_tipo );

      }

      // return view('pages/temas/admin-temas');
  }

  public function usuarioFiltro(Request $request)
  {
    
    $tipoUsuario = new TipoUser();
    $tipousuarios = $tipoUsuario->all();
    
        $users = DB::table('ovausuarios')
        ->join('ovatipousuario','ovausuarios.id_tipo','=','ovatipousuario.id')
        ->orWhere('ovausuarios.nombres', 'like', "%$request->termino%")
        ->orWhere('ovausuarios.apellidos','like', "%$request->termino%")
        ->orWhere('ovausuarios.cedula', 'like', "%$request->termino%")
        ->orWhere('ovausuarios.nick', 'like', "%$request->termino%")
        ->orWhere('ovausuarios.email', 'like', "%$request->termino%")
        ->orWhere('ovatipousuario.tipo', 'like', "%$request->termino%")
        ->select('ovausuarios.*', 'ovatipousuario.tipo as nombre_tipo_usuario')
        ->get();

        return View::make('pages/usuarios/admin-usuarios')->with('users', $users)->with('tipousuarios', $tipousuarios )->with('valorfiltro', 0 );

  }

  


  public function editUser()
  {

  }

  public function goEditUsuarios($id)
  {
      $user = User::find($id);
      $tipoUsuario = new TipoUser();
      $tipousuarios = $tipoUsuario->all();
      return view('pages/usuarios/modificarUsuario')->with('user',$user)->with('tipousuarios',$tipousuarios);
  }


  public function editarUsuarios(Request $request){
      $user = User::find($request->id);
      $user->nick=$request->get('nick');
      $user->password=$request->get('password');
      $user->nombres=$request->get('nombres');
      $user->apellidos=$request->get('apellidos');
      $user->cedula=$request->get('cedula');
      $user->email=$request->get('email');
      $user->profesion=$request->get('profesion');
      $user->carrera=$request->get('carrera');
      $user->profesion=$request->get('profesion');
      $user->semestre=$request->get('semestre');
      $user->id_tipo=$request->get('id_tipo');
      $user->status=$request->get('status');
      $user->update();
      return redirect()->route('adminusuarios')->with('success','Registro creado satisfactoriamente');
      //return response()->json($request->id);
  }


  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

    $user = new User();
    // $user->id
    $user->nick = $request->nick;
    $user->password = bcrypt($request->password);
    $user->nombres = $request->nombres;
    $user->apellidos = $request->apellidos;
    $user->cedula = $request->cedula;
    $user->email = $request->email;
    $user->profesion = $request->profesion;
    $user->carrera = $request->carrera;
    $user->semestre = $request->semestre;
    $user->id_tipo = $request->id_tipo;
    $user->status = 1;
    //$user->last_session = $request->nick;
    //$user->token = $request->nick;
    //$user->token_password = $request->nick;
    //$user->password_request = $request->nick;
    $user->save();
    //return view('pages/usuarios/admin-usuarios');
    return redirect()->route('adminusuarios')->with('success','Registro creado satisfactoriamente');

  }



}
