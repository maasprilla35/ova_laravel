<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Asignature;
class AsignaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $asignature = new Asignature();
        $asignatures = $asignature->all();
        $asignatures=$asignatures[0];
        return View::Make('pages/asignaturas/admin-asignaturas')->with('asignatures', $asignatures);
    }


    public function goAsignaturasUpdate($id)
    {
      $asignature = Asignature::find($id);
      return view('pages/asignaturas/modificarasignatura')->with('asignature',$asignature);
    }

    public function edit(Request $request){
        $asignature = Asignature::find($request->id);
        $asignature->codigo=$request->get('codigo');
        $asignature->nombre=$request->get('nombre');
        $asignature->prerequisitos=$request->get('prerequisitos');
        $asignature->programa=$request->get('programa');
        $asignature->update();
        Session::flash('alert-success', 'Asignatura editado exitosamente!');
        return redirect()->route('adminasignaturas')->with('success','Registro creado satisfactoriamente');
        //return response()->json($request->id);
    }
}
