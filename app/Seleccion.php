<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seleccion extends Model
{
  protected $table='ovaseleccion';
  protected $fillable=[
    'id',
    'enunciado',
    'opcion1',
    'opcion2',
    'opcion3',
    'opcion4',
    'opcioncorrecta',
    'tipo',
    'numopciones',
    'valor',
    'orden',
    'id_tema',
    'numerogrupo',
    'status',
    'categoria'
    
  ];
  public $timestamps = false;
  

  protected $guarded=[

  ];
}
