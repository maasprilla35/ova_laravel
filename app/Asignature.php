<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignature extends Model
{
  protected $table='ovaasignatura';
  protected $fillable=[
    'id',
    'codigo',
    'nombre',
    'prerequisitos',
    'programa'
  ];
  public $timestamps = false;
  

  protected $guarded=[

  ];
}
