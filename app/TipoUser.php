<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUser extends Model
{
  protected $table='ovatipousuario';
  protected $fillable=[
    'id',
    'tipo'
  ];
  public $timestamps = false;
  

  protected $guarded=[

  ];
}
