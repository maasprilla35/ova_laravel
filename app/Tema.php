<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
  protected $primaryKey = 'id';
  protected $table='ovatemas';
  protected $fillable=[
    'titulo',
    'introduccion',
    'prerequisitos',
    'status',
    'id_areac',
    'ayuda',
    'orden',
    'titulovideo',
    'enlacevideo',
    'tituloaudio',
    'archivoaudio',
    'tituloexterno',
    'contenidoexterno'
  ];
  public $timestamps = false;


  protected $guarded=[

  ];
}
