<!DOCTYPE html>
<html lang="es">
<head>		
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset("css/bootstrap.min.css") }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset("css/ovamiestilo.css") }}"/>
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("css/colores.php") }}"/> -->
    <link media="all" rel="stylesheet" type="text/css" href="/colores">
    <script src="{{ asset("js/jquery-3.2.0.min.js") }}"></script>
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <title>Asignatura</title>
</head>
<body>
    <div class="container-fluid">
        <nav class='navbar-default navbar-fixed-top'>
            <div class='cintilloudo'></div>
        </nav>
        <div class="row">
            <div class="imgsliderinicio">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contenedorbotonesverovaacinicio">		
                    <div class="zonamembreteinicio">
                        <img class='logoudoheaderinicio' src="{{ asset("imagenes/iconotm.svg") }}">
                        <div>
                            <span class='membrete'>MEMBRETE 1</span>
                            <span class='membrete'>MEMBRETE 2</span>
                            <span class='membrete'>MEMBRETE 3</span>
                        
                        </div>
                        <div class="asignaturainicio">
                            <div class="aprende">APRENDE EN UN CLICK</div>
                                <label for="">Hola click</label>
                            </div>							
                    </div>	
                    <a href="/ovas" class="btninicio">
                            INICIAR
                    </a>						
                </div>						
            </div>
        </div>	
        <div class="row txtbienvenida">
            Bienvenidos a los Objetos Virtuales de Aprendizaje dedicado a la enseñanza de algo, 
            donde aprenderás de forma totalmente online todo lo relacionado con el tema y además podrás 
            reforzar los conocimientos que adquieras a través de actividades y evaluaciones.
        </div>
        <div class="row txtsubtemas">
            <div class="aprenderas">APRENDERÁS ACERCA DE:</div>			
            @foreach ($temas as $tema)	
            <p style="display: inline-block;"><span class="glyphicon glyphicon-ok"></span>{{$tema->titulo}}</p>
            @endforeach
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container-fluid barrainferior">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbartema">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbartema">
                    <ul class="nav navbar-nav menuinferior">
                        <li><a href="#" data-toggle="modal" data-target="#mdayuda">Ayuda</a></li>
                    </ul>
                    <ul class='nav navbar-nav navbar-right menuinferior'>
                        <li><a href="#" data-toggle="modal" data-target="#mdcreditos">Créditos</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>	
    <!-- Modal -->
    <div class="modal fade" id="mdayuda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Ayuda</h4>
                </div>
                <div class="modal-body">
                    <p ALIGN="justify">Este es un Objeto Virtual de Aprendizaje (OVA) </p>
                        
                        <p ALIGN="justify">Para comenzar a interectuar con el OVA debe seleccionar el botón <b>INICIAR</b>, para empezar las consultas de contenido y el aprendizaje del tema determinado.</p>
                        
                        
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="mdcreditos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Créditos</h4>
                </div>
                <div class="modal-body">
                    <p ALIGN="justify">Objeto Virtual de Aprendizaje (OVA) </p><br/>
                        
                        <p ALIGN="justify">Realizado por:</p>
                                <p ALIGN="justify">CREDITOS AQUI</p>		
                </div>
            </div>
        </div>
    </div>
</body>
</html>								