@if(!Auth::check())
<nav class="navbar-default navbar-fixed-top">
		<div class="cintilloudo"></div><div class="container-fluid">
		<div class="navbar-header">
		<div class="izqmenuprincipal"><img class="logoudoheader" src="{{ asset("/imagenes/iconoac.svg") }}"><span class="datosasignaturamenuprincipal"><a class="enlaceindex" href="/"><div class="asignaturamenuprincipal">Asignatura 1</div><div class="codigomenuprincipal">000-1234
		</div></a></span><div class="lineamenuprincipal"></div></div><button type="button" class="navbar-toggle collapsed menuhamburguesasuperior" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Menú</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse menuprincipalopciones"><ul class="nav navbar-nav"></ul><ul class="nav navbar-nav navbar-right dermenuprincipal">
			<li><a href="/login">Iniciar sesión</a></li></ul></div></div></nav>

@else


			<nav class="navbar-default navbar-fixed-top">
		<div class="cintilloudo"></div><div class="container-fluid">
		<div class="navbar-header">
		<div class="izqmenuprincipal"><img class="logoudoheader" src="{{ asset("/imagenes/iconoac.svg") }}"><span class="datosasignaturamenuprincipal"><a class="enlaceindex" href="/"><div class="asignaturamenuprincipal">Asignatura 1</div><div class="codigomenuprincipal">000-1234
		</div></a></span><div class="lineamenuprincipal"></div></div><button type="button" class="navbar-toggle collapsed menuhamburguesasuperior" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Menú</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse menuprincipalopciones"><ul class="nav navbar-nav"><li><a href="/usuarios">Usuarios</a></li>
			<li><ul class="nav nav-tabs"><li class="dropdown"><a class="dropdown-toggle opcioncontenido" data-toggle="dropdown" href="#">Contenido<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="/asignaturas">Asignatura</a></li>
					<li><a href="/areasconocimientos">Áreas de conocimiento</a></li>
					<li><a href="{{ url('/temas') }}">Temas</a></li>
					<li><a href="/glosarios">Glosario</a></li>
					<li><a href="/bibliografias">Bibliografía</a></li></ul></li></ul></li>
			
			<li class=""><a href="/administrardiseno">Diseño</a></li>
			<li class=""><a href="/ovas">Ver OVA</a></li></ul>			
			<ul class="nav navbar-nav navbar-right dermenuprincipal">
			<li class="active"><a href="/areadeusuario" data-toggle="tooltip" title="Área de usuario"><span class="glyphicon glyphicon-user"></span></a></li>		
			<li><a href="/">Cerrar Sesión</a></li>
			</ul></div></div></nav>

@endif
