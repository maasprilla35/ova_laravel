<nav class="navbar navbar-inverse navbar-fixed-bottom">
				<div class="container-fluid barrainferior">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbartema">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span> 
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbartema">
						<ul class="nav navbar-nav menuinferior">
							<li><a href="#" data-toggle="modal" data-target="#mdglosario">Glosario</a></li>
							<li><a href="#" data-toggle="modal" data-target="#mdbibliografia">Bibliografía</a></li>
							<li><a href="#" data-toggle="modal" data-target="#mdayuda">Ayuda</a></li>
							@if (Session::has('video-tema'))
								@if($temas->titulovideo != null)
									<li><a href="#" data-toggle="modal" data-target="#videos">Video</a></li>
								@endif
							@endif
							@if (Session::has('enlace-tema'))
								@if($temas->tituloexterno != null)
									<li><a href="#" data-toggle="modal" data-target="#enlaceexterno">Enlaces</a></li>
								@endif
							@endif
							@if (Session::has('evaluacion-tema'))
									<li><a href="/temas/realizarevaluacion/{{$temas->id}}">Evaluacion</a></li>
							@endif
						</ul>
						<ul class="nav navbar-nav navbar-right menuinferior">
							<li><a href="#" data-toggle="modal" data-target="#mdcreditos">Créditos</a></li>
						</ul>
					</div>
				</div>
			</nav>


					<!-- Modal -->
		<div class="modal fade" id="mdayuda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Ayuda</h4>
					</div>
					<div class="modal-body">

					@if (Session::has('ayuda-content'))
							{!! $temas->ayuda !!}
					@else
							<p ALIGN="justify">Este es un Objeto Virtual de Aprendizaje (OVA) en apoyo al proceso didactico de Programación Lineal.</p>
							
							<p ALIGN="justify">Para consultas de contenido elige una de las áreas que se presenta en la entrada del OVA, ubicada en el centro de la página.</p>
							
							<p ALIGN="justify">También se encuentra en la parte superior derecha el botón de <button> Iniciar Sesión </button> para entrar como estudiante o profesor y obtener los privilegios que le consede el OVA.</p>
					@endif

					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="mdcreditos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Créditos</h4>
					</div>
					<div class="modal-body">
						<p ALIGN="justify">Objeto Virtual de Aprendizaje (OVA) </p><br/>
							
							<p ALIGN="justify">CREDITOS AQUI</p>
							
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="mdbibliografia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Bibliografía</h4>
					</div>
					<div class="modal-body">
						<div class="row table-responsive tablacentrada">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Título</th>
										<th>Autor(es)</th>
										<th>Más información</th>										
									</tr>						
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<!-- Modal -->
		<div class="modal fade" id="mdglosario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Glosario</h4>						
						<ul class="nav nav-tabs">
							
							<?php $letras="abcdefghijklmnopqrstuvwxyz";
								for($i=0;$i<strlen($letras);$i++)
								{ ?>
								
								<li <?php if($letras[$i]=='a'){?>class="bntglosario active"<?php }else{?>class="bntglosario"<?php } ?>><a data-toggle="tab" href="#letra<?php echo $letras[$i];?>"><?php echo $letras[$i];?></a></li>
								
							<?php } ?>
						</ul>							
						<div class="tab-content">
							<?php $letras="abcdefghijklmnopqrstuvwxyz";
								for($i=0;$i<strlen($letras);$i++)
								{ ?>
								
								<div id="letra<?php echo $letras[$i];?>" <?php if($letras[$i]=='a'){?>class="tab-pane fade in active"<?php }else{?> class="tab-pane fade"<?php }?>>
									<div class="row table-responsive tablacentrada">
										<table class="table table-striped">
											<tbody>									
											</tbody>
										</table>
									</div>								
								</div>								
							<?php } ?>
						</div>								
					</div>
					<div class="modal-body">						
					</div>
				</div>
			</div>
		</div>	

		@if (Session::has('video-tema'))
							<!-- Modal -->
		<div class="modal fade" id="videos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">{{$temas->titulovideo}}</h4>
					</div>
					<div class="modal-body">

					<iframe width="100%" height="100%"
						src="{{$temas->enlacevideo}}">
					</iframe>

					</div>
				</div>
			</div>
		</div>
		@endif

		@if (Session::has('enlace-tema'))
							<!-- Modal -->
		<div class="modal fade" id="enlaceexterno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">{{$temas->tituloexterno}}</h4>
					</div>
					<div class="modal-body">

						{!! $temas->contenidoexterno !!}

					</div>
				</div>
			</div>
		</div>
		@endif