@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Evaluaciones</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="container">
      <div class="row titulodetemaadmin">
        <a href="/temas" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>

        Tema: <span>Tema 2</span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12 text-left colseguntam">
        <div class="row">
          <div class="col-lg-12">
            <a href="/temas/crearEvaluacion/{{$tema->id}}" class="btn botonnuevo btnseguntam">Nueva Pregunta</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container contenedortabla">
  <div class="row table-responsive tablacentrada">
    <table class="table table-striped">
      <thead>
        <tr>
          <th><a >Enunciado</a></th>
          <th><a >Tipo</a></th>
          <th><a >Número de opciones</a></th>
          <th><a >Valor</a></th>
          <th class="colstatus"><a >Status</a></th>
          <th></th>
          <th></th>
          </tr>
        </thead>
        <tbody id="tfilas">
          @foreach ($secc as $secc )
          <tr>
            <td>{!! $secc->enunciado !!}</td>
            <td>{{$secc->nombActividad}}</td>
            <td>{{$secc->numopciones}}</td>
            <td>{{$secc->valor}}</td>
            @if($secc->status==1)
            <td class="colstatus"><a href="{!! route('cambiarEstadoEvaluacion', $secc->id) !!}"><span data-toggle="tooltip" title="Tema activo" class="glyphicon glyphicon-ok-circle"></span></a></td>
            @else
            <td class="colstatus"><a href="{!! route('cambiarEstadoEvaluacion', $secc->id) !!}"><span data-toggle="tooltip" title="Tema activo" class="glyphicon glyphicon-ban-circle"></span></a></td>
            @endif
            <td><a href="/temas/editarevaluacion/{{$tema->id}}/{{$secc->id}}" data-toggle="tooltip" title="Editar Evaluacion"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="" data-target="#modal-delete-{{$secc->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a></td>
          </tr>
          @include('pages.temas.modalEliminarEvaluacion')

          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
    @endif

  @endsection

  @section('page_scripts')
  <script src="{{ asset("tinymce/js/tinymce/tinymce.min.js") }}"></script>
  <script type='text/javascript'>
  tinymce.init({
    selector: 'textarea',
    plugins: 'image textcolor hr lists link',
    toolbar1: 'undo redo | cut copy paste | formatselect alignleft aligncenter alignright alignjustify outdent indent | bullist numlist link unlink hr image',
    toolbar2: 'fontselect fontsizeselect forecolor backcolor bold italic underline subscript superscript | removeformat',
    menubar: false,
    images_upload_url: 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile',
    images_upload_handler: function (blobInfo, success, failure) {
          var xhr, formData;
        
  
  
  
          xhr = new XMLHttpRequest();
          xhr.withCredentials = false;
          xhr.open('POST', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile');
        
          xhr.onload = function() {
              var json;
          
              if (xhr.status != 200) {
                  failure('HTTP Error: ' + xhr.status);
                  return;
              }
          
              json = JSON.parse(xhr.responseText);
          
              if (!json || typeof json.location != 'string') {
                  failure('Invalid JSON: ' + xhr.responseText);
                  return;
              }
          
              success(json.location);
          };
      
      console.log('imagen');
      console.log(blobInfo.blob());
      console.log(blobInfo.filename());
      formData = new FormData();
      formData.append('_token', "{{ csrf_token() }}");
          formData.append('file', blobInfo.blob(), blobInfo.filename());
        
          xhr.send(formData);
      },    
      themes: 'modern',
    branding: false,
    elementpath: false,
    statusbar: false,
    language: 'es',
  });
  </script>
  @endsection
