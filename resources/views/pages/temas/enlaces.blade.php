@extends('layouts.blank')

@section('main_container')

<div class="imgslidertmcn">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
		<div class="container tituloadmin">Administración de Enlaces</div>
	</div>
</div>
<div class="container">
	<div class="row areasuperioradmin">
		<div class="container">
			<div class="row titulodetemaadmin">
				<a href="administrartemas.php" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>

				Tema: <span>Tema 2</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left colseguntam">
				<div class="row">
					<input type="hidden" id="id" name="id" value="{{$tema->id}}">
					<div class="">
						<a href="/temas/crearEnlace/{{$tema->id}}" class="btn botonnuevo btnenlaces btnseguntam">Nuevo enlace</a>
					</div>
					<div class="">
						<a href="/temas/irCrearVideo/{{$tema->id}}" class="btn botonnuevo btnenlaces btnseguntam">Video</a>
					</div>
					<div class="">
						<a href="" class="btn botonnuevo btnenlaces btnseguntam">Audio</a>
					</div>
					<div class="">
						<a href="/temas/irCrearExterno/{{$tema->id}}" class="btn botonnuevo btnenlaces btnseguntam">Externo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container contenedortabla">
	<div class="row table-responsive tablacentrada">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><a href="administrarenlaces.php?orden=titulo&amp;id=11">Título</a></th>
					<th><a href="administrarenlaces.php?orden=url&amp;id=11">URL</a></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody id="tfilas">
				@foreach($enlace as $enlace)
				<tr>
					<td>{{$enlace->titulo}}</td>
					<td>{{$enlace->url}}</td>
					<td><a href="/temas/irEditarURl/{{$enlace->id}}" data-toggle="tooltip" title="Editar enlace"><span class="glyphicon glyphicon-pencil"></span></a></td>
					<td><a href="" data-target="#modalEliminarEnlace-delete-{{$enlace->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a></td>
				</tr>
				@include('pages.temas.modalEliminarEnlace')
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
    @endif
	
@endsection
