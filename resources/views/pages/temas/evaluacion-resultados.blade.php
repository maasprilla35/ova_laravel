@extends('layouts.blank')

@section('main_container')
<head>
	<link type="text/css" rel="stylesheet" href="{{ asset("css/ovaactmiestilo.css") }}"/>
</head>

<div class="row">
				<div class="imgslidertm imgslidertmcn">					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos">
						<div><a >Inicio</a> · <a >{{$temas->tituloArea}}</a> · <a class="enlaceprincipal">{{$temas->titulo}}</a></div>
					</div>
				</div>
			</div>



			<div class="container">			
			<div class="row areasuperioradmin">
				<div class="container">
					<div class="row titulodetemaadmin">
						<a href="tema.php?id=26" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
						<span>Evaluación</span>
					</div>
				</div>
											<div class="col-md-12">	
							<div class="actividadcuadrocompleto">
								<div class="actividadresultadosuperior"></div>
								<div class="row actividadresultado">
									<div class="actividadresumen">RESÚMEN DE RESULTADOS OBTENIDOS:</div>									


									<div class="resumenpadre"><div class="actividadresumencuadrototal">Respuestas correctas:<span class="actividadrespuestatotalresumen">{{$correctas}}/{{$numevaluaciones}}</span></div><div class="actividadresumencuadrototal">Respuestas incorrectas:<span class="actividadrespuestatotalresumen">{{$incorrectas}}/{{$numevaluaciones}}</span></div><div class="actividadresumencuadrototal">Total de puntos obtenidos:<span class="actividadrespuestatotalresumen">{{$valoracertado}}/{{$sumatoriavalor}}</span></div></div>

									@foreach ($evaluaciones as $evaluacion)
									<?php $opcion=$loop->iteration-1; ?>
									@if($evaluacion->tipo==2)
									<div class="actividadresumenfila"><div class="actividadencabzadofilaresumen"><span id="cuadronumeroresumen1" class="cuadradoalrededor cuadronumresumen" style="background-color: red;">{{ $loop->iteration }}</span><div class="actividadenunciadoresumen"><p>
									<span style="font-family: arial, helvetica, sans-serif; font-size: medium; text-align: justify;">{!! $evaluacion->enunciado !!}</span>
									</p></div></div><div>
									@if($arr[$opcion]==1)
									Tu respuesta:<span class="actividadrespuestaresumen">Verdadero</span>
									@else
									Tu respuesta:<span class="actividadrespuestaresumen">Falso</span>
									@endif
									</div>
									<div>
									@if($evaluacion->opcioncorrecta==$arr[$opcion])
										Revisión:<span class="actividadrespuestaresumen"><script>document.getElementById('cuadronumeroresumen1').style.backgroundColor='red';</script>Correcta</span>
									@else
										Revisión:<span class="actividadrespuestaresumen"><script>document.getElementById('cuadronumeroresumen1').style.backgroundColor='red';</script>Incorrecta</span>
									@endif
									</div>
									<div>
									@if($evaluacion->opcioncorrecta==1)
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion1}}</span>
									@else
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion2}}</span>
									@endif
									</div></div>

									@endif

									@if($evaluacion->tipo==1)
									<div class="actividadresumenfila"><div class="actividadencabzadofilaresumen"><span id="cuadronumeroresumen1" class="cuadradoalrededor cuadronumresumen" style="background-color: red;">{{ $loop->iteration }}</span><div class="actividadenunciadoresumen"><p>
									<span style="font-family: arial, helvetica, sans-serif; font-size: medium; text-align: justify;">{!! $evaluacion->enunciado !!}</span>
									</p></div></div><div>

									@if($arr[$opcion]==1)
									Tu respuesta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion1}}</span>
									@elseif($arr[$opcion]==2)
									Tu respuesta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion2}}</span>
									@elseif($arr[$opcion]==3)
									Tu respuesta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion3}}</span>
									@else
									Tu respuesta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion4}}</span>
									@endif

									</div>
									<div>
									@if($evaluacion->opcioncorrecta==$arr[$opcion])
										Revisión:<span class="actividadrespuestaresumen"><script>document.getElementById('cuadronumeroresumen1').style.backgroundColor='red';</script>Correcta</span>
									@else
										Revisión:<span class="actividadrespuestaresumen"><script>document.getElementById('cuadronumeroresumen1').style.backgroundColor='red';</script>Incorrecta</span>
									@endif
									</div>
									<div>
									@if($evaluacion->opcioncorrecta==1)
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion1}}</span>
									@elseif($evaluacion->opcioncorrecta==2)
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion2}}</span>
									@elseif($evaluacion->opcioncorrecta==3)
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion3}}</span>
									@else
										Respuesta correcta:<span class="actividadrespuestaresumen">{{$evaluacion->opcion4}}</span>
									@endif
									</div></div>
									</div></div>

									@endif


									@endforeach

								</div>
							</div>
										</div>							
			</div>
@endsection


	@section('page_scripts')
	
	@endsection