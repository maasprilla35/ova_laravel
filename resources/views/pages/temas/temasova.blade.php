@extends('layouts.blank')

@section('main_container')


<div class="row">
	<div class="imgslider">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contenedorbotonesverovaac">
			@foreach ($temas as $tema)
			<a class="abotonac" href="/temas/ver/{{$tema->id}}">
				<div class="botonac">
					<img class="iconoac" src="/imagenes/iconoac.svg">
					<p class="textobotonac">{{$tema->titulo}}</p>
					<div class="lineabotonac"></div>
					<div class="fondobotonac"></div>
					<div class="fondobotonacizq"></div>
					<div class="fondobotonacder"></div>
				</div>
			</a>
			@endforeach
			<div class="divauxinferior"></div>
		</div>
	</div>
</div>


@endsection
