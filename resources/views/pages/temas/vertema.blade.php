@extends('layouts.blank')

@section('main_container')

<div class="row">
				<div class="imgslidertm imgslidertmcn">					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos" style="width: 120%;">
						<div><a href="/" >Inicio</a> · <a href="/temas/areas/{{$temas->id_areac}}">{{$temas->tituloArea}}</a> <a href="/temas/areas/{{$temas->id_areac}}" class=""  data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a> {{$temas->titulo}}</div>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="col-md-3">
					<div class="menulateral">
						<ul class="nav nav-pills nav-stacked opcionesmenulateral">
							@foreach ($subtemas as $subtema)
							<li id="opcionml1" class="opcionmenulateral" role="presentation" onclick="selectitem(subtema_{{$subtema->id}})" onmouseenter="encimaopcionlateral(1)" onmouseleave="saliropcionlateral(1)" onclick="clicopcionlateral(1)" style="border-left-color: rgb(0, 110, 214); background-color: rgb(238, 238, 238);"><a href="#" style="color: rgb(0, 110, 214);">{{$subtema->titulo}}</a></li>	
							@endforeach				
						</ul>
					</div>
				</div>
				
				@foreach ($subtemas as $subtema)
					@if($loop->iteration == 1)
				<div class="col-md-9 contenedorcontenido SubtemasContent" id="subtema_{{$subtema->id}}" style="min-height:40em">	
					<div class="contenidoopcionlateral" id="1" style="display: inherit;">	
							<h1><p class="titulobotonregresar">{{$subtema->titulo}}</p></h1>
							<div class="contentViwer">{!! $subtema->contenido !!}</div>
					</div>					 
				</div>
					@else
					<div class="col-md-9 contenedorcontenido SubtemasContent" id="subtema_{{$subtema->id}}" style="display:none;min-height:40em">	
					<div class="contenidoopcionlateral" id="1" style="display: inherit;">	
							<h1><p class="titulobotonregresar">{{$subtema->titulo}}</p></h1>
							<div class="contentViwer">{!! $subtema->contenido !!}</div>
					</div>					 
					</div>
					@endif
				@endforeach				

			</div>


@endsection


	@section('page_scripts')
		<script src="{{ asset("tinymce/js/tinymce/tinymce.min.js") }}"></script>
		<script type='text/javascript'>
			tinymce.init({
				selector : 'div.contentViwer',
				inline: true,
				readonly: 1
			});

			var cualomlseleccionada="1";
				var opc=document.getElementById("opcionml1");
				var opca=document.getElementById("opcionml1").getElementsByTagName('a')[0];

			function saliropcionlateral(cual){
				if(cual!=cualomlseleccionada){
					var opc=document.getElementById("opcionml"+cual);
					opc.style.borderLeftColor="rgba(121,155,181,0.6)";
					var opca=document.getElementById("opcionml"+cual).getElementsByTagName('a')[0];
					opca.style.color="#5b778b";
				}
			}

			function encimaopcionlateral(cual){
				if(cual!=cualomlseleccionada){
					for (var i=1;i<numopcioneslaterales;i++){ 
					if(i!=cualomlseleccionada){
						var opc=document.getElementById("opcionml"+i);
						opc.style.backgroundColor="#eeeeee";
						opc.style.borderLeftColor="rgba(121,155,181,0.6)";
					}
					}
				
					var opc=document.getElementById("opcionml"+cual);
					opc.style.borderLeftColor="#2d3a44";
					var opca=document.getElementById("opcionml"+cual).getElementsByTagName('a')[0];
					opca.style.color="#2d3a44";
				}
			}
			
			function clicopcionlateral(cual) {
				cualomlseleccionada=cual;
				for (var i=1;i<numopcioneslaterales;i++){ 
					var diveste = document.getElementById(i.toString());
					diveste.style.display = "none";
					var opc=document.getElementById("opcionml"+i);
					opc.style.backgroundColor="#eeeeee";
					opc.style.borderLeftColor="rgba(121,155,181,0.6)";
					var opca=document.getElementById("opcionml"+i).getElementsByTagName('a')[0];
					opca.style.color="#5b778b";
				}
				var diveste = document.getElementById(cual);
				diveste.style.display = "inherit";
				var opc=document.getElementById("opcionml"+cual);
				var opca=document.getElementById("opcionml"+cual).getElementsByTagName('a')[0];
			}

			function selectitem(item){
				console.log(item);
				console.log($('.SubtemasContent'));
				$('.SubtemasContent').css('display','none');
				$(item).css('display','inline-block');
			}
		     

		</script>
	@endsection