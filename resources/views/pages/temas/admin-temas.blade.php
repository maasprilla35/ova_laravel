@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Temas</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-2 col-sm-1 col-xs-12 text-left colseguntam">
      <div class="row">
        <div class="col-lg-12">
          <a href="temas/create" class="btn botonnuevo btnseguntam">Nuevo tema</a>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-10 col-sm-11 text-right colseguntam">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right colseguntam">

          <form action="{{url('/temas/filtro')}}" method="POST" class="selectortipo btnseguntam">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <input class="selector selector2 btnseguntamtermbus btnmenospad" type="text" id="termino" name="termino" placeholder="Término a buscar">
            <input type="submit" id="enviar" name="enviar" value="Buscar" class="btn botonbuscar btnseguntambtnbus btnmenospad">
          </form>
          <form action="{{url('/temas/filtroTipo')}}" method="POST" class="selectortipo btnseguntam">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <select class="form-control selector selector2 btnseguntam btnmenospad" id="id_areac" name="id_areac" onchange="this.form.submit()">
              @if($valuefilter == 0)
              <option value="0" selected>Todos</option>
              @else
              <option value="0">Todos</option>
              @endif
              @foreach ($areas as $area)
                @if($valuefilter == $area->id)
                  <option value="{{$area->id}}" selected>{{$area->titulo}}</option>
                @else
                  <option value="{{$area->id}}">{{$area->titulo}}</option>
                @endif
              @endforeach
            </select>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="contenedortabla">
    <div class="row table-responsive tablacentrada">
      <table class="table table-striped">
        <thead>
          <tr>
            <th><a href="administrartemas.php?orden=titulo">Título</a></th>
            <th><a href="administrartemas.php?orden=id_areac">Área</a></th>
            <th class="colstatus"><a href="administrartemas.php?orden=status">Status</a></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tfilas">
          @foreach ($temas as $tema)
          <tr>
            <td>{{$tema->titulo}}</td>
            <td>{{$tema->tituloArea}}</td>
            @if($tema->status==1)
            <td class="colstatus"><a href="{!! route('temacambiarestado', $tema->id) !!}"><span data-toggle="tooltip" title="Tema activo" class="glyphicon glyphicon-ok-circle"></span></a></td>
            @else
            <td class="colstatus"><a href="{!! route('temacambiarestado', $tema->id) !!}"><span data-toggle="tooltip" title="Tema activo" class="glyphicon glyphicon-ban-circle"></span></a></td>
            @endif
            <td><a href="/subtemas/{{$tema->id}}" data-toggle="tooltip" title="Administrar subtemas del tema"><span class="glyphicon glyphicon-th-list"></span></a></td>
            <!--<td><a href="administrarimagenes.php?id=11" data-toggle="tooltip" title="Administrar imágenes del tema"><span class="glyphicon glyphicon-picture"></span></a></td>-->
            <td><a href="/temas/enlaces/{{$tema->id}}" data-toggle="tooltip" title="Administrar enlaces del tema"><span class="glyphicon glyphicon-link"></span></a></td>
            <td><a href="/temas/editAyuda/{{$tema->id}}" data-toggle="tooltip" title="Administrar ayuda del tema"><span class="glyphicon glyphicon-question-sign"></span></a></td>
            <td><a href="modificarPDF.php?id=11" data-toggle="tooltip" title="Administrar archivo PDF del tema"><span class="glyphicon glyphicon-file"></span></a></td>
            <td><a href="/temas/actividad/{{$tema->id}}" data-toggle="tooltip" title="Administrar actividades del tema"><span class="glyphicon glyphicon-tower"></span></a></td>
            <td><a href="/temas/evaluacion/{{$tema->id}}" data-toggle="tooltip" title="Administrar evaluación del tema"><span class="glyphicon glyphicon-ok"></span></a></td>
            <td><a href="/temas/ver/{{$tema->id}}" target="_blank" data-toggle="tooltip" title="Ver tema"><span class="glyphicon glyphicon-eye-open"></span></a></td>
            <td><a href="/temas/edit/{{$tema->id}}" data-toggle="tooltip" title="Editar tema"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="" data-target="#modal-delete-{{$tema->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a></td>
          </tr>
          @include('pages.temas.modal')

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>


    @if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
    @endif

    

  @endsection
