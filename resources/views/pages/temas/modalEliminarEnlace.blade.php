<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modalEliminarEnlace-delete-{{$enlace->id}}">

  {{Form::open(array('action'=>array('Admin\TemaController@deleteUrl',$enlace->id),'method'=>'post'))}}

  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="close">
          <span>X</span>
        </button>
        <h4 class="modal-tittle">Eliminar {{$enlace->titulo}}</h4>
      </div>

      <div class="modal-body">
        <p>¿Esta seguro?</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Confirmar</button>
      </div>

    </div>
  </div>
  {{form::close()}}
</div>
