@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Contenido Externo</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="container infotemaaux">
      <div class="row titulodetemaadmin">
        <a href="/temas/enlaces/{{$tema->id}}" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
        Tema: <span>{{$tema->titulo}}</span>
      </div>
    </div>
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario">
          <div>Modificación de contenido externo</div>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{url('/temas/crearExterno')}}" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />

            <div class="form-group">
              <label for="tituloexterno" class="col-md-3 control-label">Título del contenido externo</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="tituloexterno" name="tituloexterno" placeholder="Título del contenido externo" value="{{$tema->tituloexterno}}">
              </div>
            </div>
            <div class="form-group">
              <label for="contenidoexterno" class="col-md-3 control-label">Enlace del contenido externo</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="contenidoexterno" name="contenidoexterno" placeholder="Enlace del contenido externo" value="{{$tema->contenidoexterno}}">
              </div>
            </div>
            <input type="hidden" id="id" name="id" value="{{$tema->id}}">
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn">Guardar</button>
                <a onclick="borrar();" href="#" class="btn btnborrar">Borrar</a>
                <a href="/temas/enlaces/{{$tema->id}}" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function borrar(){
  var campoeste = document.getElementById("titulovideo");
  campoeste.value="";
  var campoeste = document.getElementById("enlacevideo");
  campoeste.value="";
}
</script>

@endsection
