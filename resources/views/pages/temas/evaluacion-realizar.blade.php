@extends('layouts.blank')

@section('main_container')
<head>
	<link type="text/css" rel="stylesheet" href="{{ asset("css/ovaactmiestilo.css") }}"/>
</head>

<div class="row">
				<div class="imgslidertm imgslidertmcn">					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos">
					<div><a >Inicio</a> · <a >{{$temas->tituloArea}}</a> · <a  class="enlaceprincipal">{{$temas->titulo}}</a></div>
					</div>
				</div>
			</div>


			<div class="container">			
			<div class="row areasuperioradmin">
				<div class="container">
					<div class="row titulodetemaadmin">
						<a href="tema.php?id=27" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
						<span>Evaluación</span>
					</div>
				</div>
						<form id="ejercicioform" class="form-horizontal" role="form" action="{{url('/temas/resultadosTemas')}}" method="POST" autocomplete="off">			
							<input type="hidden" value="{{csrf_token()}}" name="_token" />
							<input type="hidden" value="{{$idTema}}" name="id_tema" />

							<div class="col-md-12">	
								<div class="actividadcuadrocompleto">
									<div class="actividadcuadrosuperior"></div>
									<div class="row actividadcuadro">
										<div class="actividadinstrucciones enunciadoinstrucciones" id="instrucciones">Lea detenidamente cada enunciado presentado y seleccione la respuesta correcta para cada uno:</div>
									</div>

									@foreach ($evaluaciones as $evaluacion)
										<div class="actividadcuadrosuperior"></div>
											<div class="row actividadcuadro">
												<!-- Enunciado -->
												<div class="actividadresumenfila">
													<div class="actividadencabzadofilaresumen">
														<span id="cuadronumeroresumen1" class="cuadradoalrededor cuadronumresumen">{{ $loop->iteration }}</span>
														<div class="actividadenunciadoresumen" style="width: 100%;">
															<p>{!! $evaluacion->enunciado !!}</p>

															@if($evaluacion->tipo==2)
															<!-- Opciones -->
															<div class="actividadopciones">
																<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-1" for="opcion{{ $loop->iteration }}-1" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);">
																<div class="actividadopcion">
																	<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-1" value="1">
																</div>
																Verdadero</div></label>
																<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-2" for="opcion{{ $loop->iteration }}-2" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);">
																<div class="actividadopcion">
																	<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-2" value="2">
																</div>
																Falso</div></label>
															</div>
															@endif

															@if($evaluacion->tipo==1)
																@if($evaluacion->numopciones==3)
<div class="actividadopciones">
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-1" for="opcion{{ $loop->iteration }}-1" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-1" value="1"></div>
															{{$evaluacion->opcion1}}													</div>
														</label>
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-2" for="opcion{{ $loop->iteration }}-2" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-2" value="2"></div>
															{{$evaluacion->opcion2}}														</div>
														</label>
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-3" for="opcion{{ $loop->iteration }}-3" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-3" value="3"></div>
															{{$evaluacion->opcion3}}													</div>
														</label>
																									</div>

																@else

																<div class="actividadopciones">
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-1" for="opcion{{ $loop->iteration }}-1" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-1" value="1"></div>
															{{$evaluacion->opcion1}}														</div>
														</label>
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-2" for="opcion{{ $loop->iteration }}-2" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-2" value="2"></div>
															{{$evaluacion->opcion2}}														</div>
														</label>
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-3" for="opcion{{ $loop->iteration }}-3" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-3" value="3"></div>
															{{$evaluacion->opcion3}}														</div>
														</label>
																											<label onclick="clickradio()" class="enunciadoevaluacion" id="etiqueta{{ $loop->iteration }}-4" for="opcion{{ $loop->iteration }}-4" style="border-color: rgb(224, 224, 224); color: rgb(128, 128, 128);"><div class="actividadopcion">
															<div class="radioevaluacion"><input type="radio" name="opcion{{ $loop->iteration }}" id="opcion{{ $loop->iteration }}-4" value="4"></div>
															{{$evaluacion->opcion4}}														</div>
														</label>
																									</div>



																@endif
															@endif

														</div>	
													</div>																	
												</div>																			
											</div>
										</div>
									
									@endforeach
									<div class="actividadpiecuadroeval">
										<input style="display:none" id="btncorregir" class="btngeneral btncorregir btncorregireval" type="submit" value="REVISAR" disabled />
									</div>	
								</div>
							</div>
						</form>																
			</div>							
			</div>
@endsection


	@section('page_scripts')
		<script>

$(document).ready(function(){
		var maxnumeva={{$numevaluaciones}};
		var crespondida=[];
		nrespondidas=0;		
		for (k = 0; k < maxnumeva; k++) {
			crespondida[k]=0;
		}
		for (j = 1; j <= maxnumeva; j++) {	
			for (i = 1; i <= 4; i++) {
				var elemento = document.getElementById("opcion"+j+"-"+i);
				if(elemento!==null){
					$(elemento).change(function () {
						str=this.id;
						var res = str.split("opcion");
						var res = res[1].split("-");
						cres=parseInt(res[0]);
						if(crespondida[cres-1]==0){
							nrespondidas++;
							crespondida[cres-1]=1;
						}		
						if(nrespondidas==maxnumeva){
							document.getElementById("btncorregir").disabled=false;	
							document.getElementById("btncorregir").style.display="inherit";
						}
					});
				}
			}
	}
	});


						$('.enunciadoevaluacion').on('mouseover', function() {
					nmb=this.id;
					nmb = nmb.replace("etiqueta", "opcion");
					opc=document.getElementById(nmb);
					if(!opc.checked){
						$(this).css("border-color", "#333333");
						$(this).css("color", "#333333");
					}
				});
				$('.enunciadoevaluacion').on('mouseout', function() {
					nmb=this.id;
					nmb = nmb.replace("etiqueta", "opcion");
					opc=document.getElementById(nmb);
					if(!opc.checked){
						$(this).css("border-color", "#e0e0e0");
						$(this).css("color", "#808080");
					}
				});

				function clickradio(){
					var maxnumeva={{$numevaluaciones}};
					for (jc = 1; jc <= maxnumeva; jc++) {
						for (i = 1; i <= 4; i++) {				
							var opc=document.getElementById("opcion"+jc+"-"+i);
							if(opc!==null){
								var etq=document.getElementById("etiqueta"+jc+"-"+i);
								etq.style.borderColor="#e0e0e0";
								etq.style.color="#808080";
								
								if(opc.checked){
									var colorprincipal="{{$diseno->colorprincipal}}";
									etq.style.borderColor=colorprincipal;
									etq.style.color=colorprincipal;
								}
							}
						}
					}
				}
		</script>
	@endsection