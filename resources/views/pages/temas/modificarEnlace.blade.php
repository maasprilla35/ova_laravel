@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Enlaces</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="container infotemaaux">
      <div class="row titulodetemaadmin">
        <a href="administrarenlaces.php?id=11" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
        Tema: <span>Tema 2</span>
      </div>
    </div>
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario" class="">
          <div class="">Registro de enlace</div>
        </div>
        <div class="panel-body">
          <form id="signupform" class="form-horizontal" role="form" action="{{url('/temas/editarURl')}}" method="POST" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />

            <div id="signupalert" style="display:none" class="alert alert-danger">
              <p>Error:</p>
              <span></span>
            </div>
            <div class="form-group">
              <label for="titulo" class="col-md-3 control-label">Título</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Título" value="{{$enlace->titulo}}" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="url" class="col-md-3 control-label">URL</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="url" id="url" placeholder="URL" value="{{$enlace->url}}" required="">
              </div>
            </div>
            <input type="hidden" id="id" name="id" value="{{$enlace->id}}">
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                <button id="btn-signup" type="submit" class="btn"><i class="icon-hand-right"></i>Registrar</button>
                <a href="administrarenlaces.php?id=11" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
