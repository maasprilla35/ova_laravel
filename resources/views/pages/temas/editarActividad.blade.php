@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">MODIFICACIÓN DE ACTIVIDAD</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="container infotemaaux">
      <div class="row titulodetemaadmin">
        <a href="administraractividades.php?id=11" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
        Tema: <span>Tema 2</span>
      </div>
    </div>
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario">
          <div>MODIFICACIÓN DE ACTIVIDAD</div>
        </div>
        <div class="panel-body">
          <form id="signupform" class="form-horizontal" role="form" action="{{url('/temas/editActividad')}}" method="POST" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <input type="hidden" value="{{$seleccion->id}}" name="id" />
            <input type="hidden" value="{{$tema->id}}" name="idtema" />
            <div id="signupalert" style="display:none" class="alert alert-danger">
              <p>Error:</p>
              <span></span>
            </div>
            <div class="form-group">
              <label for="enunciado" class="col-md-3 control-label">Enunciado</label>
              <div class="col-md-9">
                <textarea rows="20" class="form-control enunciadocorto" id="enunciado" name="enunciado" placeholder="Enunciado" aria-hidden="true">{{$seleccion->enunciado}}</textarea>
              </div>
            </div>

            @if($seleccion->tipo==1)
            <div id="tiposeleccion" >
              <div class="form-group">
                <label for="numopciones" class="col-md-3 control-label">Número de opciones</label>
                <div class="col-md-9">
                  <select class="form-control" id="numopciones" name="numopciones">
                    @if($seleccion->numopciones==3)
                      <option value="3" selected>3</option>
                      <option value="4">4</option>
                    @else
                      <option value="3">3</option>
                      <option value="4" selected>4</option>
                    @endif
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="opcion1" class="col-md-3 control-label">Opción 1</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="opcion1" placeholder="Opción 1" value="<?php echo $seleccion->opcion1; ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="opcion2" class="col-md-3 control-label">Opción 2</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="opcion2" placeholder="Opción 2" value="<?php echo $seleccion->opcion2; ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="opcion3" class="col-md-3 control-label">Opción 3</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="opcion3" placeholder="Opción 3" value="<?php echo $seleccion->opcion3; ?>">
                </div>
              </div>
              @if($seleccion->numopciones==3)
              <div id="cuartaopcion" class="form-group" style="display:none;">
                <label for="opcion4" class="col-md-3 control-label">Opción 4</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="opcion4" placeholder="Opción 4" value="<?php echo $seleccion->opcion4; ?>">
                </div>
              </div>
              @endif

              @if($seleccion->numopciones==4)
              <div id="cuartaopcion" class="form-group" >
                <label for="opcion4" class="col-md-3 control-label">Opción 4</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="opcion4" placeholder="Opción 4" value="<?php echo $seleccion->opcion4; ?>">
                </div>
              </div>
              @endif
              <div id="3opciones" class="form-group">
                <label for="opcioncorrecta3o" class="col-md-3 control-label">Opción correcta</label>
                <div class="col-md-9">
                  <select class="form-control" id="opcioncorrecta3o" name="opcioncorrecta3o">
                    @if($seleccion->opcioncorrecta == 1)
                      <option value="1" selected>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                    @elseif($seleccion->opcioncorrecta == 2)
                      <option value="1">1</option>
                      <option value="2" selected>2</option>
                      <option value="3">3</option>
                    @else
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3" selected>3</option>
                    @endif
                  </select>
                </div>
              </div>
              <div id="4opciones" class="form-group" style="display:none;">
                <label for="opcioncorrecta4o" class="col-md-3 control-label">Opción correcta</label>
                <div class="col-md-9">
                  <select class="form-control" id="opcioncorrecta4o" name="opcioncorrecta4o">
                @if($seleccion->opcioncorrecta == 1)
                  <option value="1" selected>1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                @elseif($seleccion->opcioncorrecta == 2)
                  <option value="1">1</option>
                  <option value="2" selected>2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                @elseif($seleccion->opcioncorrecta == 3)
                  <option value="1">1</option>
                  <option value="2" selected>3</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                @else
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4" selected>4</option>
                @endif
                  </select>
                </div>
              </div>
            </div>
            @else
            <div id="tipovof" class="form-group" >
              <label for="opcioncorrectavof" class="col-md-3 control-label">Opción correcta</label>
              <div class="col-md-9">
                <select class="form-control" id="opcioncorrectavof" name="opcioncorrectavof">
                  @if($seleccion->opcioncorrecta==1)
                    <option value="1" selected>Verdadero</option>
                    <option value="2">Falso</option>
                  @else
                    <option value="1">Verdadero</option>
                    <option value="2" selected>Falso</option>
                  @endif
                </select>
              </div>
            </div>
            @endif


            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">

                <button id="btn-signup" type="submit" class="btn"><i class="icon-hand-right"></i>Registrar</button>
                <a href="/temas/evaluacion/{{$tema->id}}" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<script language='javascript'>

$(document).ready(function(){
  $("#tipo").change(function () {
    seleccion = $(this).val();
    switch(seleccion) {
      case '0':
      var diveste = document.getElementById("tiposeleccion");
      diveste.style.display = "none";
      var diveste = document.getElementById("tipovof");
      diveste.style.display = "none";
      break;
      case '1':
      var diveste = document.getElementById("tiposeleccion");
      diveste.style.display = "inherit";
      var diveste = document.getElementById("tipovof");
      diveste.style.display = "none";
      break;
      case '2':
      var diveste = document.getElementById("tiposeleccion");
      diveste.style.display = "none";
      var diveste = document.getElementById("tipovof");
      diveste.style.display = "inherit";
      break;
    }
  });
});



$(document).ready(function(){
  $("#numopciones").change(function () {
    seleccion = $(this).val();
    switch(seleccion) {
      case '3':
      var diveste = document.getElementById("cuartaopcion");
      diveste.style.display = "none";
      var diveste = document.getElementById("3opciones");
      diveste.style.display = "inherit";
      var diveste = document.getElementById("4opciones");
      diveste.style.display = "none";
      break;
      case '4':
      var diveste = document.getElementById("cuartaopcion");
      diveste.style.display = "inherit";
      var diveste = document.getElementById("3opciones");
      diveste.style.display = "none";
      var diveste = document.getElementById("4opciones");
      diveste.style.display = "inherit";
      break;
    }
  });
});
</script>


@endsection

@section('page_scripts')
<script src="{{ asset("tinymce/js/tinymce/tinymce.min.js") }}"></script>
<script type='text/javascript'>
tinymce.init({
  selector: '#enunciado',
  plugins: 'image textcolor hr lists link',
  toolbar1: 'undo redo | cut copy paste | formatselect alignleft aligncenter alignright alignjustify outdent indent | bullist numlist link unlink hr image',
  toolbar2: 'fontselect fontsizeselect forecolor backcolor bold italic underline subscript superscript | removeformat',
  menubar: false,
	images_upload_url: 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile',
	images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      



        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
		
		console.log('imagen');
		console.log(blobInfo.blob());
		console.log(blobInfo.filename());
		formData = new FormData();
		formData.append('_token', "{{ csrf_token() }}");
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },  
    themes: 'modern',
  branding: false,
  elementpath: false,
  statusbar: false,
  language: 'es',
});
</script>
@endsection
