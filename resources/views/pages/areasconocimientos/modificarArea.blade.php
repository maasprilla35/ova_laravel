@extends('layouts.blank')

@section('main_container')
<div class="imgslidertmcn">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
    <div class="container tituloadmin">Administración de Áreas de Conocimiento</div>
  </div>
</div>

<div class="container">
  <div class="row areasuperioradmin">
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario" class="modificacion">
          <div class="">Modificación de área de conocimiento</div>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{url('/areasconocimientos/update')}}" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />

            <div class="form-group">
              <label for="titulo" class="col-md-3 control-label">Título</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" value="{{$area->titulo}}" required="">
              </div>
            </div>
            <input type="hidden" id="id" name="id" value="{{$area->id}}">
            <div class="form-group">
              <label for="status" class="col-md-3 control-label">Status</label>
              <div class="col-md-9">
                @if($area->status==1)
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="1" checked=""> Activo
                </label>
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="0"> Inactivo
                </label>
                @else
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="1" =""> Activo
                </label>
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="0" checked> Inactivo
                </label>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn">Guardar</button>
                <a href="/areasconocimientos" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
