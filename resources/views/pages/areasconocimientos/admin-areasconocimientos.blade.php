@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Áreas de Conocimiento</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12 text-left colseguntam">
      <div class="row">
        <div class="col-lg-12">
          <a href="areasconocimientos/create" class="btn botonnuevo btnseguntam">Nuevo área de conocimiento</a>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-10 text-right colseguntam">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right colseguntam">
          <a href="ordenar.php?qo=ac&amp;to=OVA" class="btn botonbuscar btnseguntam">Ordenar áreas de conocimiento</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="contenedortabla">
    <div class="row table-responsive tablacentrada">
      <table class="table table-striped">
        <thead>
          <tr>
            <th><a href="administrarareasc.php?orden=titulo">Título</a></th>
            <th class="colstatus"><a href="administrarareasc.php?orden=status">Status</a></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tfilas">
          @foreach ($areas as $area)
          <tr>
            <td>{{$area->titulo}}</td>
            @if($area->status==1)
            <td class="colstatus">
              <a href="{!! route('areccambiarestado', $area->id) !!}">
                <span data-toggle="tooltip" title="Área de conocimiento activo" class="glyphicon glyphicon-ok-circle"></span>
              </a>
            </td>
            @else
            <td class="colstatus"><a href="{!! route('areccambiarestado', $area->id) !!}"><span data-toggle="tooltip" title="Área de conocimiento inacactivo" class="glyphicon glyphicon-ban-circle"></span></a></td>
            @endif
            <td><a href="administrartemas/areas/{{$area->id}}" data-toggle="tooltip" title="Administrar temas del área de conocimiento"><span class="glyphicon glyphicon-th-large"></span></a></td>
            <td><a href="areasconocimientos/edit/{{$area->id}}" data-toggle="tooltip" title="Editar área de conocimiento"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="" data-target="#modal-delete-{{$area->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></a></td>
            </tr>
          @include('pages.areasconocimientos.modal')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
  @endif
@endsection
