@extends('layouts.blank')

@section('main_container')


<div class="row">
	{{Auth::check()}}
	<div class="imgslider">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contenedorbotonesverovaac">
			@foreach ($areas as $area)
			<a class="abotonac" href="temas/areas/{{$area->id}}">
				<div class="botonac">
					<img class="iconoac" src="/imagenes/iconoac.svg">
					<p class="textobotonac">{{$area->titulo}}</p>
					<div class="lineabotonac"></div>
					<div class="fondobotonac"></div>
					<div class="fondobotonacizq"></div>
					<div class="fondobotonacder"></div>
				</div>
			</a>
			@endforeach
			<div class="divauxinferior"></div>
		</div>
	</div>
</div>


@endsection
