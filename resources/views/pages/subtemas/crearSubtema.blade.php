@extends('layouts.blank')

@section('main_container')
<div class="imgslidertmcn">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
		<div class="container tituloadmin">Administración de Subtemas</div>
	</div>
</div>
<div class="container">
	<div class="row areasuperioradmin">
		<div class="container infotemaaux">
			<div class="row titulodetemaadmin">
				<a href="/subtemas/{{$tema->id}}" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>
				Tema: <span>Tema 2</span>
			</div>
		</div>
		<div id="signupbox" class="col-lg-12">
			<div class="formulario">
				<div id="cabeceraformulario" class="">
					<div class="">Registro de subtema</div>
				</div>
				<div class="panel-body">
					<form id="signupform" class="form-horizontal" role="form" action="{{url('/subtemas/store')}}" method="POST" autocomplete="off">
						{{Form::token()}}

						<div id="signupalert" style="display:none" class="alert alert-danger">
							<p>Error:</p>
							<span></span>
						</div>
						<div class="form-group">
							<label for="titulo" class="col-md-3 control-label">Título</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="titulo" placeholder="Título" value="" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="contenido" class="col-md-3 control-label">Contenido</label>
							<div class="col-md-9">
								<textarea rows="20" class="form-control enunciadolargo" id="contenido" name="contenido" placeholder="Contenido" aria-hidden="true" ></textarea>
							</div>
						</div>
						<input type="hidden" id="id" name="id" value="{{$tema->id}}">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">

								<button id="btn-signup" type="submit" class="btn"><i class="icon-hand-right"></i>Registrar</button>
								<a href="/subtemas/{{$tema->id}}" class="btn">Cancelar</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('page_scripts')
<script src="{{ asset("tinymce/js/tinymce/tinymce.min.js") }}"></script>
<script type='text/javascript'>
tinymce.init({
  selector: 'textarea',
  plugins: 'image textcolor hr lists link',
  toolbar1: 'undo redo | cut copy paste | formatselect alignleft aligncenter alignright alignjustify outdent indent | bullist numlist link unlink hr image',
  toolbar2: 'fontselect fontsizeselect forecolor backcolor bold italic underline subscript superscript | removeformat',
  menubar: false,
  images_upload_url: 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile',
  images_upload_handler: function (blobInfo, success, failure) {
	  var xhr, formData;
	



	  xhr = new XMLHttpRequest();
	  xhr.withCredentials = false;
	  xhr.open('POST', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/uploadfile');
	
	  xhr.onload = function() {
		  var json;
	  
		  if (xhr.status != 200) {
			  failure('HTTP Error: ' + xhr.status);
			  return;
		  }
	  
		  json = JSON.parse(xhr.responseText);
	  
		  if (!json || typeof json.location != 'string') {
			  failure('Invalid JSON: ' + xhr.responseText);
			  return;
		  }
	  
		  success(json.location);
	  };
	  
	  console.log('imagen');
	  console.log(blobInfo.blob());
	  console.log(blobInfo.filename());
	  formData = new FormData();
	  formData.append('_token', "{{ csrf_token() }}");
	  formData.append('file', blobInfo.blob(), blobInfo.filename());
	
	  xhr.send(formData);
  },  
  themes: 'modern',
  branding: false,
  elementpath: false,
  statusbar: false,
  language: 'es',
});
</script>
@endsection