@extends('layouts.blank')

@section('main_container')
<div class="imgslidertmcn">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
		<div class="container tituloadmin">Administración de Subtemas</div>
	</div>
</div>

<div class="container">
	<div class="row areasuperioradmin">
		<div class="container">
			<div class="row titulodetemaadmin">
				<a href="/temas" data-toggle="tooltip" title="Regresar"><span class="glyphicon glyphicon-chevron-left"></span></a>

				Tema: <span>Tema 2</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-2 col-xs-12 text-left colseguntam">
				<div class="row">
					<div class="col-lg-4">
						<a href="/subtemas/create/{{$tema->id}}" class="btn botonnuevo btnseguntam">Nuevo subtema</a>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-10 text-right colseguntam">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right colseguntam">
						<a href="ordenar.php?qo=st&amp;ct=11&amp;to=Tema 2" class="btn botonbuscar btnseguntam">Ordenar subtemas</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="container contenedortabla">
	<div class="row table-responsive tablacentrada">
		<table class="table table-striped tablalistado">
			<thead>
				<tr>
					<th><a href="temaregistrado.php?orden=titulo&amp;id=11">Título</a></th>
					<th class="colstatus"><a href="temaregistrado.php?orden=status&amp;id=11">Status</a></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody id="tfilas">
				@foreach($subtema as $subtema )
				<tr>
					<td>{{$subtema->titulo}}</td>
					<td class="colstatus"><a href="cambiarstatussubtema.php?id=16&amp;tm=11"><span data-toggle="tooltip" title="Subtema activo" class="glyphicon glyphicon-ok-circle"></span></a></td>
					<td><a href="/subtemas/edit/{{$subtema->id}}" data-toggle="tooltip" title="Editar tema"><span class="glyphicon glyphicon-pencil"></span></a></td>
					<td><a href="" data-target="#modal-delete-{{$subtema->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></a></td>
					</tr>
					@include('pages.subtemas.modal')
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	@endsection
