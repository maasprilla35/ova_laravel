
@extends('layouts.blank')

@section('main_container')
<div class="container">


<div class="row areasuperioradmin">
	<div id="signupbox" class="col-lg-12">
		<div class="formulario">
			<div id="cabeceraformulario" class="modificacion">
				<div class="">Modificación de diseño</div>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" method="POST" action="{{url('modificardiseno/update')}}" autocomplete="off">
					<input type="hidden" value="{{csrf_token()}}" name="_token" />

					<div class="form-group">
						<label for="codigo" class="col-md-3 control-label"><!--<div class="colorprincipal colormod"></div> -->Color principal</label>
						<div class="col-md-9">
							<input type="color" class="form-control" id="colorprincipal" name="colorprincipal" placeholder="Color principal" value="{{$diseno->colorprincipal}}" required="">
						</div>
					</div>
					<div class="form-group">
						<label for="nombre" class="col-md-3 control-label">Color resaltado</label>
						<div class="col-md-9">
							<input type="color" class="form-control" id="colorsecundario" name="colorsecundario" placeholder="Color resaltado" value="{{$diseno->colorsecundario}}" required="">
						</div>
					</div>
					<input type="hidden" id="id" name="id" value="1">
					<div class="form-group">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn">Guardar</button>
							<a href="administrardiseno.php" class="btn">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</div>
@endsection
