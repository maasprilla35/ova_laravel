@extends('layouts.blank')

@section('main_container')



<div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
  <div class="formulariologin">
    <div class="panel-body">
      <form id="loginform" class="form-horizontal" role="form" action="{{url('/inicioSesion')}}" method="POST" autocomplete="off">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="input-group areacampotexto">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
          <input id="nick" type="text" class="campotexto" name="nick" value="" placeholder="Nick o email" required="">
        </div>
        <div class="input-group areacampotexto">
          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input id="password" type="password" class="campotexto" name="password" placeholder="Password" required="">
        </div>
        <div class="form-group" style="margin-top: 35px">
          <div class="col-sm-12 controls">
            <button id="btn-login" type="submit" class="boton">INICIAR SESIÓN</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


@if (Session::has('alert-warning'))
    <div id="error" class="alert alert-danger" role="alert">
			<ul><li>La contraseña es incorrecta</li></ul>
    </div>
@endif

@endsection
