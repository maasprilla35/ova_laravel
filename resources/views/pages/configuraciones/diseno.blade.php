
@extends('layouts.blank')

@section('main_container')

<div class="row">
				<div class="imgslidertmcn">					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
						<div class="container tituloadmin">Administración de Diseño</div>
					</div>
				</div>
			</div>
			
<div class="container">
	<div class="row areasuperioradmin">
		<div class="col-lg-4 col-md-4 col-sm-2 col-xs-12 text-left colseguntam">
			<div class="row">
				<div class="col-lg-4">
					<a href="/modificardiseno" class="btn botonnuevo btnseguntam">Modificar diseño</a>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="contenedortabla info">
		<div class="row">
			<div class="col-lg-1"><div class="colorprincipal"></div></div><div class="col-lg-3 colortitulo">Color principal: </div><div class="col-lg-10 colorcodigo">{{$diseno->colorprincipal}}</div>
		</div>
		<div class="row">
			<div class="col-lg-1"><div class="colorsecundario"></div></div><div class="col-lg-3 colortitulo">Color resaltado: </div><div class="col-lg-10 colorcodigo">{{$diseno->colorsecundario}}</div>
		</div>
	</div>
</div>
@endsection
