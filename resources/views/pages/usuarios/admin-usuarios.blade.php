@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Usuarios</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12 text-left colseguntam">
      <div class="row">
        <div class="col-lg-12 filaovf">
          <a href="/usuarios/create" class="btn botonnuevo btnseguntam">Nuevo usuario</a></div>
        </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-10 text-right colseguntam">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right colseguntam">
            <form action="{{url('/usuarios/filtro')}}" method="POST" class="selectortipo btnseguntam">
              <input type="hidden" value="{{csrf_token()}}" name="_token" />
              <input class="selector btnseguntamtermbus" type="text" id="termino" name="termino" placeholder="Término de búsqueda">
              <input type="submit" id="enviar" name="enviar" value="Buscar" class="btn botonbuscar btnseguntambtnbus">
            </form>

            <form action="{{url('/usuarios/filtroTipo')}}" method="POST" class="selectortipo btnseguntam">
              <input type="hidden" value="{{csrf_token()}}" name="_token" />
              <select class="form-control selector btnseguntam" id="id_tipo" name="id_tipo" onchange="this.form.submit()">
                @if($valorfiltro==0)
                  <option value="0" selected>Todos</option>
                @else
                  <option value="0" >Todos</option>
                @endif

                @foreach ($tipousuarios as $tipousuario)
                  @if($tipousuario->id == $valorfiltro)
                    <option value="{{$tipousuario->id}}" selected>  {{$tipousuario->tipo}}</option>               
                  @else
                    <option value="{{$tipousuario->id}}">  {{$tipousuario->tipo}}</option>                
                  @endif
                @endforeach
              </select>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="container">
    <div class="contenedortabla">
      <div class="row table-responsive tablacentrada">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><a href="administrarusuarios.php?orden=nombres">Nombres</a></th>
              <th><a href="administrarusuarios.php?orden=apellidos">Apellidos</a></th>
              <th><a href="administrarusuarios.php?orden=cedula">Cédula</a></th>
              <th><a href="administrarusuarios.php?orden=nick">Nick</a></th>
              <th><a href="administrarusuarios.php?orden=email">Email</a></th>
              <th><a href="administrarusuarios.php?orden=id_tipo">Tipo</a></th>
              <th class="colstatus"><a href="administrarusuarios.php?orden=status">Status</a></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="tfilas">

            <script>var cborrar=[]; nfilaj=1;</script>

            @foreach ($users as $user)
            <tr>
              <td><strong><a href="areadeusuario.php?idu=17">{{$user->nombres}}</a></strong></td>
              <td><strong><a href="areadeusuario.php?idu=17">{{$user->apellidos}}</a></strong></td>
              <td>{{$user->cedula}}</td>
              <td>{{$user->nick}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->nombre_tipo_usuario}}</td>
              <!--<td><span class="glyphicon glyphicon-cog"></span></td>-->
              <td class="colstatus"><span class="glyphicon glyphicon-ok-circle deshabilitado"></span></td>
              <td><a href="usuarios/edit/{{$user->id}}" data-toggle="tooltip" title="Editar usuario"><span class="glyphicon glyphicon-pencil"></span></a></td>
              <td><span id="fila1" class="glyphicon glyphicon-trash deshabilitado"></span><script>cborrar[nfilaj]="Usuario admin"; nfilaj=nfilaj+1;</script></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  @endsection
