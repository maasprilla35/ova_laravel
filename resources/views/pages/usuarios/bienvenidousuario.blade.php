@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Área de Usuario</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
			<div class="contenedortabla info">
				<div class="row">
					<div class="col-lg-6">
						<div class="row datos bienvenida">
              <div class="col-lg-12">Bienvenido(a)</div>
              <div class="col-lg-12">{{$users->nombres}} {{$users->apellidos}}</div>
              <div class="col-lg-12">Administrador</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row datos datosusuario">
            <div class="col-lg-3 dutitulo">Alias</div><div class="col-lg-9 duinfo">{{$users->nick}}</div>
            <div class="col-lg-3 dutitulo">Cédula</div><div class="col-lg-9 duinfo">{{$users->cedula}}</div>
            <div class="col-lg-3 dutitulo">Correo</div><div class="col-lg-9 duinfo">{{$users->email}}</div>
            <div class="col-lg-3 dutitulo">Profesión</div><div class="col-lg-9 duinfo">{{$users->profesion}}</div>																					</div>
					</div>
				</div>
				<div class="row revresumen" style="    text-align: center;        border-top: 3px solid #DDA0E8;">
					<div class="col-lg-12">
						<div class="noevaluacion" style="    font-size: 22px;">Aún no ha realizado ninguna evaluación</div>					</div>
				</div>
			</div>
		</div>
@endsection
