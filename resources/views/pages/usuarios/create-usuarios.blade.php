@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Usuarios</div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row areasuperioradmin">
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario" class="">
          <div class="">Registro de usuario</div>
        </div>
        <div class="panel-body">
          <form id="signupform" class="form-horizontal" role="form" action="{{url('/usuarios/store')}}" method="POST" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div id="signupalert" style="display:none" class="alert alert-danger">
              <p>Error:</p>
              <span></span>
            </div>
            <div class="form-group">
              <label for="nombres" class="col-md-3 control-label">Nombres</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="nombres" placeholder="Nombres" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="apellidos" class="col-md-3 control-label">Apellidos</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="cedula" class="col-md-3 control-label">Cédula</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="cedula" placeholder="Cédula" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="nick" class="col-md-3 control-label">Nick</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="nick" placeholder="Nick" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-md-3 control-label">Password</label>
              <div class="col-md-9">
                <input type="password" class="form-control" name="password" placeholder="Password" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-md-3 control-label">Email</label>
              <div class="col-md-9">
                <input type="email" class="form-control" name="email" placeholder="Email" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="tipo_usuario" class="col-md-3 control-label">Tipo de usuario</label>
              <div class="col-md-9">
                <select class="form-control" id="id_tipo" name="id_tipo">
                  <option value="0">Seleccione un tipo</option>

                  <option value="1">Administrador</option>

                  <option value="2">Docente</option>

                  <option value="3">Estudiante</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="divprofesion" style="display:none;">
              <label for="profesion" class="col-md-3 control-label">Profesión</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="profesion" id="profesion" placeholder="Profesión" value="">
              </div>
            </div>
            <div class="form-group" id="divcarrera" style="display:none;">
              <label for="carrera" class="col-md-3 control-label">Carrera</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="carrera" id="carrera" placeholder="Carrera" value="">
              </div>
            </div>
            <div class="form-group" id="divsemestre" style="display:none;">
              <label for="semestre" class="col-md-3 control-label">Semestre</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="semestre" id="semestre" placeholder="Semestre" value="">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                <button id="btn-signup" type="submit" class="btn"><i class="icon-hand-right"></i>Registrar</button>
                <a href="administrarusuarios.php" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
$("#id_tipo").change(function () {
  seleccion = $(this).val();
  console.log(seleccion);
  switch(seleccion) {
    case '0':
    var diveste = document.getElementById("divprofesion");
    diveste.style.display = "none";
    var diveste = document.getElementById("divcarrera");
    diveste.style.display = "none";
    var diveste = document.getElementById("divsemestre");
    diveste.style.display = "none";
    break;
    case '1':
    var diveste = document.getElementById("divprofesion");
    diveste.style.display = "inherit";
    var diveste = document.getElementById("divcarrera");
    diveste.style.display = "none";
    var diveste = document.getElementById("divsemestre");
    diveste.style.display = "none";
    break;
    case '2':
    var diveste = document.getElementById("divprofesion");
    diveste.style.display = "inherit";
    var diveste = document.getElementById("divcarrera");
    diveste.style.display = "none";
    var diveste = document.getElementById("divsemestre");
    diveste.style.display = "none";
    break;
    case '3':
    var diveste = document.getElementById("divprofesion");
    diveste.style.display = "none";
    var diveste = document.getElementById("divcarrera");
    diveste.style.display = "inherit";
    var diveste = document.getElementById("divsemestre");
    diveste.style.display = "inherit";
    break;
  }

});
</script>
@endsection
