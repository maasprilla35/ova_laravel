@extends('layouts.blank')

@section('main_container')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
	<div class="container tituloadmin">Administración de Usuarios</div>
</div>

<div class="container">
	<div class="row areasuperioradmin">
		<div id="signupbox" class="col-lg-12">
			<div class="formulario">
				<div id="cabeceraformulario" class="modificacion">
					<div class="">Modificación de usuario</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" method="POST" action="{{url('/usuarios/update')}}" autocomplete="off">
						<input type="hidden" value="{{csrf_token()}}" name="_token" />

						<div class="form-group">
							<label for="nombres" class="col-md-3 control-label">Nombres</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" value="{{$user->nombres}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="apellidos" class="col-md-3 control-label">Apellidos</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" value="{{$user->apellidos}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="cedula" class="col-md-3 control-label">Cédula</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" value="{{$user->cedula}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="nick" class="col-md-3 control-label">Nick</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="nick" name="nick" placeholder="Nick" value="{{$user->nick}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="{{$user->password}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-md-3 control-label">Email</label>
							<div class="col-md-9">
								<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email}}" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="id_tipo" class="col-md-3 control-label">Tipo de usuario</label>
							<div class="col-md-9">
								<select class="form-control" id="id_tipo" name="id_tipo">
									@foreach ($tipousuarios as $tipousuario)
									@if($tipousuario->id == $user->id_tipo)
									<option value="{{$tipousuario->id}}" selected>{{$tipousuario->tipo}}</option>
									@else
									<option value="{{$tipousuario->id}}">{{$tipousuario->tipo}}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group" id="divprofesion" style="display: inherit;">
							<label for="profesion" class="col-md-3 control-label">Profesión</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="profesion" name="profesion" placeholder="Profesión" value="{{$user->profesion}}">
							</div>
						</div>
						<div class="form-group" id="divcarrera" style="display:none;">
							<label for="carrera" class="col-md-3 control-label">Carrera</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="carrera" name="carrera" placeholder="Carrera" value="{{$user->carrera}}">
							</div>
						</div>
						<div class="form-group" id="divsemestre" style="display:none;">
							<label for="semestre" class="col-md-3 control-label">Semestre</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="semestre" name="semestre" placeholder="Semestre" value="{{$user->semestre}}">
							</div>
						</div>
						<input type="hidden" id="id" name="id" value="{{$user->id}}">
						<div class="form-group">
							<label for="status" class="col-md-3 control-label">Status</label>
							<div class="col-md-9">
								@if ($user->status == 1)
								<label class="radio-inline">
									<input type="radio" id="status" name="status" value="1" checked> Activo
								</label>
								<label class="radio-inline">
									<input type="radio" id="status" name="status" value="0"> Inactivo
								</label>
								@else($user->status == 0)
								<label class="radio-inline">
									<input type="radio" id="status" name="status" value="1" > Activo
								</label>
								<label class="radio-inline">
									<input type="radio" id="status" name="status" value="0" checked> Inactivo
								</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn">Guardar</button>
								<a href="administrarusuarios.php" class="btn">Cancelar</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
