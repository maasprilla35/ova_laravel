@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Asignatura</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-4 col-sm-2 text-left">
      <div class="row">
        <div class="col-lg-4">
          <a href="asignaturas/edit/{{$asignatures->id}}" class="btn botonnuevo btnseguntam">Modificar asignatura</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="contenedortabla info">
    <div class="row">
      <div class="col-lg-12">Código: </div><div class="col-lg-12">{{$asignatures->codigo}}</div>
    </div>
    <div class="row">
      <div class="col-lg-12">Nombre: </div><div class="col-lg-12">{{$asignatures->nombre}}</div>
    </div>
    <div class="row">
      <div class="col-lg-12">Prerequisitos: </div><div class="col-lg-12">{{$asignatures->prerequisitos}}</div>
    </div>
    <div class="row">
      <div class="col-lg-12">Contenido programático: </div><div class="col-lg-12"><p class="MsoNormal">&nbsp;</p>
        {!!html_entity_decode($asignatures->programa)!!}
        <p>&nbsp;</p></div>
      </div>
    </div>
  </div>
  @if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
  @endif
  @endsection
