@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Glosario</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-4 col-sm-2 text-left">
      <div class="row">
        <div class="col-lg-4">
          <a href="glosarios/create" class="btn botonnuevo btnseguntam">Nuevo término</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="contenedortabla">
    <div class="row table-responsive tablacentrada">
      <table class="table table-striped">
        <thead>
          <tr>
            <th><a href="administrarglosario.php?orden=termino">Término</a></th>
            <th>Definición</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tfilas">
          @foreach ($glosarios as $glosario)
          <tr>
            <td>{{$glosario->termino}}</td>
            <td>{!! $glosario->definicion !!}<span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: small;"></span></td>
            <td><a href="glosarios/edit/{{$glosario->id}}" data-toggle="tooltip" title="Editar término"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="" data-target="#modal-delete-{{$glosario->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></a></td>
            </tr>
            @include('pages.terminos.modal')
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  @if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
  @endif

  @endsection
