@extends('layouts.blank')

@section('main_container')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
  <div class="container tituloadmin">Administración de Glosario</div>
</div>

<div class="container">
  <div class="row areasuperioradmin">
    <div id="signupbox" class="col-lg-12">
      <div class="formulario">
        <div id="cabeceraformulario" class="">
          <div class="">Registro de término</div>
        </div>
        <div class="panel-body">
          <form id="signupform" class="form-horizontal" role="form" action="{{url('/glosarios/store')}}" method="POST" autocomplete="off">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div id="signupalert" style="display:none" class="alert alert-danger">
              <p>Error:</p>
              <span></span>
            </div>
            <div class="form-group">
              <label for="termino" class="col-md-3 control-label">Término</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="termino" placeholder="Término" value="" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="definicion" class="col-md-3 control-label">Definición</label>
              <div class="col-md-9">
                <textarea rows="20" class="form-control enunciadocorto" id="definicion" name="definicion" placeholder="Definición" aria-hidden="true"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">

                <button id="btn-signup" type="submit" class="btn"><i class="icon-hand-right"></i>Registrar</button>
                <a href="administrarglosario.php" class="btn">Cancelar</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
