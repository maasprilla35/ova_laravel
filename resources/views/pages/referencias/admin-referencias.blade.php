@extends('layouts.blank')

@section('main_container')

<div class="container-fluid">
  <div class="row">
    <div class="imgslidertmcn">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 secciontitulos secciontitulosadmin">
        <div class="container tituloadmin">Administración de Bibliografía</div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row areasuperioradmin">
    <div class="col-lg-4 col-md-4 col-sm-2 text-left">
      <div class="row">
        <div class="col-lg-4">
          <a href="bibliografias/create" class="btn botonnuevo btnseguntam">Nueva referencia</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="contenedortabla">
    <div class="row table-responsive tablacentrada">
      <table class="table table-striped">
        <thead>
          <tr>
            <th><a href="administrarbibliografia.php?orden=titulo">Título</a></th>
            <th><a href="administrarbibliografia.php?orden=autor">Autor(es)</a></th>
            <th>Más información</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tfilas">
          @foreach ($biblios as $biblio)
          <tr>
            <td>{{$biblio->titulo}}</td>
            <td>{{$biblio->autor}}</td>
            <td>{!! $biblio->info !!}</td>
            <td><a href="bibliografias/edit/{{$biblio->id}}" data-toggle="tooltip" title="Editar tema"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="" data-target="#modal-delete-{{$biblio->id}}" data-toggle="modal"><span class="glyphicon glyphicon-trash"></a></td>
            </tr>
            @include('pages.referencias.modal')
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>


  @if (Session::has('alert-success'))
      <div class="alert alert-success alert-dismissable fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ Session::get('alert-success') }}</strong>
      </div>
  @endif

    
  @endsection
