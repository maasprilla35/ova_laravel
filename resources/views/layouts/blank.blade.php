<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset("css/bootstrap.min.css") }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset("css/ovamiestilo.css") }}"/>
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("css/colores.php") }}"/> -->
    <link media="all" rel="stylesheet" type="text/css" href="/colores">

    <script src="{{ asset("js/jquery-3.2.0.min.js") }}"></script>
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <title>Blank</title>
</head>
<body>
    @include('includes/topbar')
    @yield('main_container')
    @include('includes/footer')
    @yield('page_scripts')

</body>
</html>